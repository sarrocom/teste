<?php 

require_once("../includes/cabecalho.php");
require_once("../includes/menu.php");

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Consultas</h1>
			</div>
			<div class="col-md-4">
                <p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
                <a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
            </div>
		</div>
	</div>	
</div>

<div id="consulta">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 " style="margin-top: 50px;">
				<div class="row">
					<div class="col-sm-4 consultaDoDia" >
						<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
						<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
						<button class="btn btn-default">Ficha</button>
					</div>
					<div class="col-sm-4 consultaDoDia" >
						<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
						<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
						<button class="btn btn-default">Ficha</button>
					</div>
					<div class="col-sm-4 consultaDoDia" >
						<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
						<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
						<button class="btn btn-default">Ficha</button>
					</div>
				</div>
				<hr>
			</div>
				
			<hr class="divisao">

			<div class="col-md-3 aside" >

				<div class="outrasOpcoes">
					<a href="consultas.php" class="btn botoesDefault anotacoes">Anotações</a>
					<a href="consulta_avaliacao-medica.php" class="btn botoesDefault">Avaliação médica</a>
					<a href="consulta_produtos.php" class="btn botoesDefault" >Selecione Produtos</a>
					<a href="consulta_posologia.php" class="btn botoesDefault">Posologia</a>
					<a href="consulta_prescricao.php" class="btn botoesDefault">Prescrição</a>
					<a href="consulta_laudo-medico.php" class="btn botoesDefault">Laudo médico</a>
				</div>
				
			</div>
				
			


			<div class="col-md-9 main" >
				
				<div class="col-sm-12 col-md-12 avaliacaoMedica">
					<div class="row">
						<div class="col-md-12">
							<h6 class="tituloSeccao">Produtos</h6>
						</div>
					</div>

					<form action="">
						<div class="row">
							<div class="col-sm-4" style="padding-top: 30px; text-align: right;">Selecione a enferemidade</div>
							<div class="col-md-5">
								<div class="form-group" style="margin-top: 15px;">
									<select name="" id="" class="enfermidade">
										<option value="Enfermidade">Enfermidade </option>
										<option value="Enfermidade01">Enfermidade01</option>
										<option value="Enfermidade02">Enfermidade02</option>
									</select>
								</div>	
							</div>
							<div class="col-md-3">
								<div class="form-group botaoSalvar">
									<button type="submit" class="btn botoesDefault">Buscar</button>
								</div>
							</div>
						</div>
					</form>
					
					<div class="">
						<h6 class="tituloSeccao">Produtos em destaque</h6>
					</div>
					<form action="">
						<div class="consultaProdutos">
						
							<div class="row">
								<div class="col-md-4" >
									<div class="itemProduto">
										<img src="http://via.placeholder.com/250x200">
										<h5>Nome produto</h5>
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
											Informações
										</button>
										<button type="button" class="btn btn-success">Adicionar</button>
									</div>
								</div>
								<div class="col-md-4" >
									<div class="itemProduto">
										<img src="http://via.placeholder.com/250x200">
										<h5>Nome produto</h5>
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
											Informações
										</button>
										<button type="button" class="btn btn-success">Adicionar</button>
									</div>
								</div>
								<div class="col-md-4" >
									<div class="itemProduto">
										<img src="http://via.placeholder.com/250x200">
										<h5>Nome produto</h5>
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
											Informações
										</button>
										<button type="button" class="btn btn-success">Adicionar</button>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group botaoSalvar">
							<button type="submit" class="btn botoesDefault">Salvar</button>
						</div>

						<div class="modal fade" id="myModal">
							<div class="modal-dialog modal-dialog-centered" style="    max-width: 700px;">
						  		<div class="modal-content">
						  
						    		<!-- Modal Header -->
						    		<div class="modal-header">
						      			<h4 class="modal-title">Informações do Produto</h4>
						      		<button type="button" class="close" data-dismiss="modal">&times;</button>
						    	</div>
						    
						    	<!-- Modal body -->
						    	<div class="modal-body">
						    		<div class="row">
						    			<div class="col-lg-6" style="text-align: center;">
						    				<img src="http://via.placeholder.com/350x250" style="width: 100%;">
						    				<button class="btn btn-success" style="margin-top: 20px; width: 150px;">Adicionar</button>
						    			</div>
						    			<div class="col-lg-6">
						    				<div class="info">
						    					<h5>Nome: </h5>
						    					<p>voluptas, at consequatur?</p>
						    				</div>
						    				<hr>
						    				<div class="info">
						    					<h5>Fabricante: </h5>
						    					<p>voluptas, at consequatur?</p>
						    				</div>
						    				<hr>
						    				<div class="info">
						    					<h5>Composição: </h5>
						    					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus laboriosam harum omnis temporibus quisquam, iure, ullam deleniti, sunt quos labore</p>
						    				</div>
						    				<hr>
						    			</div>
						    		</div>	
								
									<div class="descricaoModal" style="margin-top: 30px;">
										<h5>Bula</h5>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus laboriosam harum omnis temporibus quisquam, iure, ullam deleniti, sunt quos labore beatae dicta sequi fugiat ea. Ipsa, quo incidunt. Rem, error.
									</div>
						    	</div>
						    
						    	<!-- Modal footer -->
						    	<div class="modal-footer">
						      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    	</div>
						  	</div>
						</div>
					</form>

				</div>

			</div>	
		</div>
	</div>
</div>

<script>
	//Slider
$(document).ready(function(){
  	$('.slider').slick({
  		autoplay: true,
 		autoplaySpeed: 1000,
 		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 3
  	});
	
});
</script>


<?php require_once("../includes/rodape.php");?>