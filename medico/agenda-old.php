<?php 

require_once( "../includes/cabecalho.php");
require_once( "../includes/menu.php");
require_once( '../conexao.php');

?>
<script>
	$(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            }
        });
    }
</script>
<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Sua agenda médica</h1>
			</div>
			<div class="col-md-4">
				<p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
				<a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
			</div>
		</div>
	</div>	
</div>

<div class="agenda">
	<div class="container">
	    <div class="row">
	       
	     
	        <div id="calendar" class="col-sm-8 col-md-9 animated animated-sm bounceInUp">
	           <div id='calendar'></div>  
	        </div>
	    	
			
			<div id="overview" class="col-sm-4 col-md-3">
	            <div class="headerAgendaRight">
	            	<h4>Agenda do dia</h4>
	            </div>
	          
	            <div class="conteudoAgenda">
	            	<div class="row">
						<div class="col-sm-12 consultaDoDia" style="border-bottom: 1px solid #e5e5e5;">
							<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
							<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
							<button class="btn btn-default">Ficha</button>
						</div>
						<div class="col-sm-12 consultaDoDia" style="border-bottom: 1px solid #e5e5e5;">
							<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
							<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
							<button class="btn btn-default">Ficha</button>
						</div>
						<div class="col-sm-12 consultaDoDia" style="border-bottom: 1px solid #e5e5e5;">
							<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
							<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
							<button class="btn btn-default">Ficha</button>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
</div>


<?php require_once( "../includes/rodape.php");
include_once('../admin/includes/footer.php');
?>