<?php 

require_once ("../includes/cabecalho.php");
require_once ("../includes/menu.php");
require_once ("../conexao.php");
require_once ('../sql/select.php');

$enfermidade = exibeListaEnfermidade($conexao);
?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Enfermidades</h1>
			</div>
			<div class="col-md-4">
                <p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
                <a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
            </div>
		</div>
	</div>	
</div>

<section class="container">
	<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th>ID</th>
                <th>Enfermidade</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($enfermidade as $enfermidade) :
            ?>
            <tr class="even gradeA">
                <td style="width: 40px;"><?=$enfermidade['id_enfermidade'];?></td>
                <td><?=$enfermidade['nome_enfermidade'];?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>


<?php require_once ("../includes/rodape.php");?>