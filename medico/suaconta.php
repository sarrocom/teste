<?php 

require_once ("../includes/cabecalho.php");
require_once ("../includes/menu.php");
include_once('../conexao.php');
include_once('../sql/select.php');

$id = $_GET['id'];
$medicos = exibeMedicos($conexao, $id);
//echo $id;

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Sua conta</h1>
			</div>
			<div class="col-md-4">
				<p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
				<a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
			</div>
		</div>
	</div>	
</div>

<div class="container">
	<div class="row">
		<div class="col-md-9" >
			<form name="formularioMedico" method="post" action="../recebe-forms/alteraMedico.php">
				<?php foreach ($medicos as $medico) :?>
                <input type="hidden" name="id" value="<?=$medico['id']?>">
                <div class="form-group">
                    <input class="form-control" name="nome" id="nome" value="<?=$medico['nome']?>" placeholder="NOME COMPLETO">
                    <div id="nomeC"></div>
                </div>
                <div class="form-group">
                    <input class="form-control" name="crm" id="crm" value="<?=$medico['crm']?>" placeholder="CRM">
                    <div id="crM"></div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <input class="form-control" name="email" id="email" placeholder="E-MAIL"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="<?=$medico['email']?>">
                            <div id="emaiL"></div>
                        </div>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="senha" id="senha" placeholder="SENHA" value="sistemaAnell@2018">
                            <div id="emaiL"></div>
                        </div>
                    </div>      
                </div>
                <div class="form-group">
                    <input class="form-control" name="cep" type="text" id="cep" value="<?=$medico['cep']?>" size="10" maxlength="9" placeholder="CEP" />
                    <div id="ceP"></div>
                </div>
                
                <div class="form-group" style="overflow: hidden;">
                	<div class="row">
                    <div class="col-md-4">
                        <input class="form-control" name="rua" type="text" id="rua" value="<?=$medico['endereco']?>" placeholder="ENDEREÇO"/>
                        <div id="enderecO"></div>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control" name="numero" id="numero" value="<?=$medico['numero']?>" placeholder="NUMERO">
                        <div id="numerO"></div>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control" name="complemento" type="text" id="complemento" value="<?=$medico['complemento']?>" placeholder="COMPLEMENTO">
                    </div>
                    </div>
                </div>
                
                <div class="form-group" style="overflow: hidden;">
                	<div class="row">
	                    <div class="col-lg-6">
	                        <input class="form-control" name="cidade" type="text" id="cidade" size="40" value="<?=$medico['cidade']?>" placeholder="CIDADE"/>
	                        <div id="cidadE"></div>
	                    </div>
	                    <div class="col-lg-6">
	                        <input class="form-control" name="uf" type="text" id="uf" size="2" value="<?=$medico['estado']?>" placeholder="ESTADO"/>
	                        <div id="estadO"></div>
	                    </div>
	                </div>
                </div>

                <div class="form-group">
                    <input class="form-control" name="cpf" id="cpf" value="<?=$medico['cpf']?>" placeholder="CPF">
                    <div id="cpF"></div>
                </div>

                <div class="form-group">
                    <input class="form-control" name="telefone" id="telefone"  placeholder="TELEFONE" value="<?=$medico['telefone']?>">
                    <div id="telefonE"></div>
                </div>

                <div class="form-group" style="overflow: hidden;">
                    <div class="row">
                        <div class="col-lg-6">
                            <select class="form-control" name="areaAtuacao" style="height: 50px;">
                                <option>Selecione uma especialidade</option>
                                <?php
                                $especialidades = exibeListaEspecialidade($conexao);
                                foreach ($especialidades as $especialidade) :
                                ?>
                                <option value="<?=$especialidade['especialidade']?>"><?=$especialidade['especialidade']?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            *Lista de especialidades
                        </div>
                    </div>
                </div>

                <div class="form-group" style="margin-top: 50px;">
                    <h5 class="text-center"><strong>Selecione os horários de trabalho da semana</strong></h5>
                    <table class="table table-bordered">
                        <thead>
                            <th>Dia</th>
                            <th>Hora chegada</th>
                            <th>Hora Saída</th>
                        </thead>
                        <tr>
                            <td>Segunda</td>
                             <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioSegunda" value="<?=$medico['segunda_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimSegunda" value="<?=$medico['segunda_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                        <tr>
                            <td>Terça</td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioTerca" value="<?=$medico['terca_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimTerca" value="<?=$medico['terca_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                        <tr>
                            <td>Quarta</td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioQuarta" value="<?=$medico['quarta_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimQuarta" value="<?=$medico['quarta_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                        <tr>
                            <td>Quinta</td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioQuinta" value="<?=$medico['quinta_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimQuinta" value="<?=$medico['quinta_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                        <tr>
                            <td>Sexta</td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioSexta" value="<?=$medico['sexta_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimSexta" value="<?=$medico['sexta_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                        <tr>
                            <td>Sábado</td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioSabado" value="<?=$medico['sabado_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimSabado" value="<?=$medico['sabado_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                        <tr>
                            <td>Domingo</td>
                             <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaInicioDomingo" value="<?=$medico['domingo_chegada']?>" placeholder="Hora ínicio">
                            </td>
                            <td style="width: 150px;">
                                 <input class="form-control chegada_saida" type="text" name="horaFimDomingo" value="<?=$medico['domingo_saida']?>" placeholder="Hora Fim">
                            </td>
                        </tr>
                    </table>                            
                </div>
				
				<?php endforeach ?>

                <div class="form-group" style="text-align: center;">
                    <button class="btn btn-primary" onclick="return valida_formulario_medico()">Alterar</button>
                </div>
            </form>


		</div>
	</div>
</div>


<?php require_once ("../includes/rodape.php");?>