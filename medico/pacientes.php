<?php 
require_once ("../includes/cabecalho.php");
require_once ("../includes/menu.php");
require_once('../conexao.php');
require_once('../sql/select.php');
?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Pacientes</h1>
			</div>
			<?php require_once('../includes/sejabemvindo.php'); ?>
		</div>
	</div>	
</div>
<div class="pacientes">
	<div class="container">
		<table cellpadding="0"  class="table table-striped table-bordered" id="example">
            <thead>
                <tr>
                    <th class="text-center">Data Consulta</th>
                    <th class="text-center">Nome</th>
                    <th class="text-center">Data Nascimento</th>
                    <th class="text-center">Sexo</th>
                    <th class="text-center">E-mail</th>
                    <th class="text-center">Info</th>
                </tr>
            </thead>
            <tbody>

                <tr class="even gradeA">
                    <td class="text-center">00/00/0000</td>
                    <td class="text-center">nome</td>
                    <td class="text-center">data_nascimento</td>
                    <td class="text-center">sexo</td>
                    <td class="text-center">email</td>
                    <td class="text-center"><a href="consultas.php" >Informações</a></td>
                </tr>  
            </tbody>
        </table>
	</div>
</div>


<?php include_once "../includes/rodape.php";?>