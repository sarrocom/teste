<?php 

require_once("../includes/cabecalho.php");
require_once("../includes/menu.php");

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Consultas</h1>
			</div>
			<div class="col-md-4">
                <p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
                <a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
            </div>
		</div>
	</div>	
</div>

<div id="consulta">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 " style="margin-top: 50px;">
				<div class="row">
					<div class="col-sm-4 consultaDoDia" >
						<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
						<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
						<button class="btn btn-default">Ficha</button>
					</div>
					<div class="col-sm-4 consultaDoDia" >
						<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
						<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
						<button class="btn btn-default">Ficha</button>
					</div>
					<div class="col-sm-4 consultaDoDia" >
						<p class="consultaHorario"><b>Horário:</b> 10:00hs</p>
						<p class="consultaPaciente"><b>Paciente:</b> Lucas Carlos Lacerda</p>
						<button class="btn btn-default">Ficha</button>
					</div>
				</div>
				<hr>
			</div>
				
			<hr class="divisao">

			<div class="col-md-3 aside" >

				<div class="outrasOpcoes">
					<a href="consultas.php" class="btn botoesDefault anotacoes">Anotações</a>
					<a href="consulta_avaliacao-medica.php" class="btn botoesDefault">Avaliação médica</a>
					<a href="consulta_produtos.php" class="btn botoesDefault" >Selecione Produtos</a>
					<a href="consulta_posologia.php" class="btn botoesDefault">Posologia</a>
					<a href="consulta_prescricao.php" class="btn botoesDefault">Prescrição</a>
					<a href="consulta_laudo-medico.php" class="btn botoesDefault">Laudo médico</a>
				</div>
				
			</div>
				
			


			<div class="col-md-9 main" >
				
				<div class="col-sm-12 col-md-12 avaliacaoMedica">
					<div class="row">
						<div class="col-md-12">
							<h6 class="tituloSeccao">Prescrição</h6>
						</div>
					</div>

					<form action="">							
						<div class="areaAvaliacao">
							<div class="row">
								<div class="col-md-12">
									
									<div class="form-group descricao">
										<label for="descricao">Redija sua prescrição</label>
										<textarea name="descricao" class="form-control" id="" cols="30" rows="10"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group botaoSalvar">
							<button type="submit" class="btn botoesDefault">Salvar</button>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>


<?php require_once("../includes/rodape.php");?>