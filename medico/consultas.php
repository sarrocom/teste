<?php 
require_once("../includes/cabecalho.php");
require_once("../includes/menu.php");
require_once('../conexao.php');
require_once('../sql/consulta.php');

$cpf = $_SESSION['cpf'];
$pacientes = pacienteConsulta($conexao, $cpf);
$anotacoes = exibeAnotacoes($conexao);

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Consultas</h1>
			</div>
			<div class="col-md-4">
                <p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
                <a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
            </div>
		</div>
	</div>	
</div>

<div id="consulta">
	<div class="container">
		<div class="row">
			<div class="col-md-3 aside" >

				<div class="outrasOpcoes">
					<a href="consultas.php" class="btn botoesDefault anotacoes">Anotações</a>
					<a href="consulta_avaliacao-medica.php" class="btn botoesDefault">Avaliação médica</a>
					<a href="consulta_produtos.php" class="btn botoesDefault" >Selecione Produtos</a>
					<a href="consulta_posologia.php" class="btn botoesDefault">Posologia</a>
					<a href="consulta_prescricao.php" class="btn botoesDefault">Prescrição</a>
					<a href="consulta_laudo-medico.php" class="btn botoesDefault">Laudo médico</a>
				</div>
				
			</div>


			<div class="col-md-9 main" >
				<?php 
				if (isset($_SESSION['msg'])) {
					echo $_SESSION['msg'];
					unset ($_SESSION['msg']);
				}
				?>
				<div class="col-sm-12 col-md-12 consulta2">
					<?php foreach ($pacientes as $paciente) : ?>
					<div class="row">
						<h3 class="nomePaciente"><?=$paciente['nome']?></h3>
						<h6 class="tituloSeccao">Dados Pessoais</h6>
					</div>
					<div class="row">
						<div class="col-md-6 dadosPessoais">
							<p><b>Data de Nascimento:</b> <?=$paciente['data_nascimento']?></p>
							<p><b>CPF:</b> <?=$paciente['cpf']?></p>
							<p><b>RG:</b> <?=$paciente['rg']?></p>
						</div>
						<div class="col-md-6 dadosPessoais">
							<p><b>E-mail:</b> <?=$paciente['email']?></p>
							<p><b>Sexo:</b> <?=$paciente['sexo']?></p>
						</div>
					</div>
					
				</div>

				<div class="col-sm-12 col-md-12 avaliacaoMedica">

						
					<h6 class="tituloSeccao">Anotações</h6>

					<form action="../recebe-forms/recebe-consulta.php" method="post">
						<input type="hidden" name="nome" value="<?=$paciente['nome']?>">
						<input type="hidden" name="cpf" value="<?=$paciente['cpf']?>">
						<input type="hidden" name="sexo" value="<?=$paciente['sexo']?>">
						<input type="hidden" name="email" value="<?=$paciente['email']?>">
						<input type="hidden" name="rg" value="<?=$paciente['rg']?>">					
						<div class="form-group">
								<textarea name="anotacoes" class="form-control" id="" cols="30" rows="10"></textarea>
							</div>
							<div class="form-group botaoSalvar">
								<button type="submit" class="btn botoesDefault">Salvar</button>
							</div>
						</div>
					</form>
					<?php endforeach ?>
					<hr>
					<?php foreach ($anotacoes as $anotacao): 
						if ($cpf == $anotacao['cpf_anotacao']) {
							$dataD = explode(" ", $anotacao['data_anotacao']);
		                    $data = date('d/m/Y', strtotime($dataD[0]));
					?>
					
						<div class="consultaDoDia" style="border-bottom: 1px solid">
							<p class="consultaHorario"><b><?=$data?> - <?=$dataD[1]?></b></p>
							<p class="consultaPaciente"><b></b>
								<?=$anotacao['anotacao']?>
							</p>
						</div>
					<?php } endforeach ?>
					</<div>	
				</div>
			</div> 

			
		</div>


	</div>
</div>



<?php require_once("../includes/rodape.php");?>