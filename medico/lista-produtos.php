<?php 

include_once "../includes/cabecalho.php";
include_once "../includes/menu.php";

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Produtos</h1>
			</div>
			<div class="col-md-4">
				<p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
				<a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
			</div>
		</div>
	</div>	
</div>

<div class="produtos">
	<div class="container">
		<div class="areaBuscaProdutos">
			<div class="row">
	  			<div class="col-md-2">
	  				
	  			</div>
	  			<div class="col-md-8">
	  				<form class="form-inline">
				   
					    <input type="search" class="form-control campoBuscaProdutos" id="pwd" placeholder="Buscar produtos" name="s">
					    
					    <button type="submit" class="botaoBuscaProdutos btn ">
					    	<i class="fas fa-search"></i>
					    </button>
					</form>
	  			</div>
	  			<div class="col-md-2">
	  				
	  			</div>	
	  		</div>	
	  	</div>
	</div>

	<div class="container">
		<div class="lista-produtos">
			<div class="row">
				<div class="col-md-3 ">
					<div class="itemProduto">
						<img src="http://via.placeholder.com/250x200">
						<h2>Nome produto</h2>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
						    Informações
						</button>
					</div>
				</div>
				<div class="col-md-3 ">
					<div class="itemProduto">
						<img src="http://via.placeholder.com/250x200">
						<h2>Nome produto</h2>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
						    Informações
						  </button>
					</div>
				</div>
				<div class="col-md-3 ">
					<div class="itemProduto">
						<img src="http://via.placeholder.com/250x200">
						<h2>Nome produto</h2>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
					    Informações
					  </button>
					</div>
				</div>
				<div class="col-md-3 ">
					<div class="itemProduto">
						<img src="http://via.placeholder.com/250x200">
						<h2>Nome produto</h2>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
					    Informações
					  </button>
					</div>
				</div>
			</div>
		</div>

	</div>




<!--
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    Informações
  </button>
-->
  <!-- The Modal -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog modal-dialog-centered" style="    max-width: 700px;">
  <div class="modal-content">
  
    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Informações do Produto</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    
    <!-- Modal body -->
    <div class="modal-body">
      	<section id="slider" class="slider">
			<div class="section-slider" style="background: blue;height: 200px;width: 333px;margin-right: 33px;width: 300px;">	 	
			 	texto 01
			 </div>

			 <div class="section-slider" style="background: grey;height: 200px;width: 333px;margin-right: 33px;width: 300px;">
			 	texto 02
			 </div>
		  	<div class="section-slider" style="background: black;height: 200px;width: 333px;margin-right: 33px;width: 300px;">
		  		texto 03
		  	</div>	
		  	<div class="section-slider" style="background: green;height: 200px;width: 333px;margin-right: 33px;width: 300px;"> 	
			 	texto 04
			 </div>
		</section>
		
		<div class="descricaoModal" style="margin-top: 30px;">
			<h3>Descrição</h3>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus laboriosam harum omnis temporibus quisquam, iure, ullam deleniti, sunt quos labore beatae dicta sequi fugiat ea. Ipsa, quo incidunt. Rem, error.
		</div>
		

    </div>
    
    <!-- Modal footer -->
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
    
  </div>
	</div>
</div>


<script>
	//Slider
$(document).ready(function(){
  	$('.slider').slick({
  		autoplay: true,
 		autoplaySpeed: 1000,
 		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 3
  	});
	
});
</script>


<?php include_once "../includes/rodape.php";?>