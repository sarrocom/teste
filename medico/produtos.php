<?php 

require_once ("../includes/cabecalho.php");
require_once ("../includes/menu.php");
require_once ('../conexao.php');
require_once ('../sql/select.php');

$produtos = exibeListaProdutos($conexao);

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Produtos</h1>
			</div>
			<?php require_once('../includes/sejabemvindo.php'); ?>
		</div>
	</div>	
</div>

<div class="produtos">
	<div class="container">
		<div class="areaBuscaProdutos">
			<div class="row">
	  			<div class="col-md-2">
	  				
	  			</div>
	  			<div class="col-md-8">
	  				<form class="form-inline">
				   
					    <input type="search" class="form-control campoBuscaProdutos" id="pwd" placeholder="Buscar produtos" name="s" style="max-width: 500px;width: 100%;">
					    
					    <button type="submit" class="botaoBuscaProdutos btn">
					    	Buscar
					    </button>
					</form>
	  			</div>
	  			<div class="col-md-2">
	  				
	  			</div>	
	  		</div>	
	  	</div>
	</div>

	<div class="container">
		<div class="lista-produtos">
			<div class="row">
				<?php foreach ($produtos as $produto) : ?>
				<div class="col-md-3">
					<div class="itemProduto">
						<img src="../fotos/<?=$produto['foto_produto']?>" alt="">
						<h5><?=$produto['nome_produto']?></h5>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
					    Informações
					  </button>
					</div>
				</div>
				<?php endforeach?>
			</div>
		</div>
	</div>

</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog modal-dialog-centered" style="    max-width: 700px;">
  		<div class="modal-content">
  
    		<!-- Modal Header -->
    		<div class="modal-header">
      			<h4 class="modal-title">Informações do Produto</h4>
      		<button type="button" class="close" data-dismiss="modal">&times;</button>
    	</div>
    
    	<!-- Modal body -->
    	<div class="modal-body">
    		<?php foreach ($produtos as $produto) : ?>
    		<div class="row">
    			<div class="col-lg-6" style="text-align: center;">
    				<img src="../fotos/<?=$produto['foto_produto']?>" alt="" style="width: 100%;">
    			</div>
    			<div class="col-lg-6">
    				<div class="info">
    					<h6>Nome: </h6>
    					<p><?=$produto['nome_produto']?></p>
    				</div>
    				<hr>
    				<div class="info">
    					<h6>Fabricante: </h6>
    					<p><?=$produto['nome_fabricante']?></p>
    				</div>
    				<hr>
    				<div class="info">
    					<h6>Composição: </h6>
    					<p><?=$produto['descricao']?></p>
    				</div>
    				<hr>
    			</div>
    		</div>	
		
			<div class="descricaoModal" style="margin-top: 30px;">
				<h6>Bula</h6>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus laboriosam harum omnis temporibus quisquam, iure, ullam deleniti, sunt quos labore beatae dicta sequi fugiat ea. Ipsa, quo incidunt. Rem, error.
			</div>
			<?PHP endforeach ?>
    	</div>
    
    	<!-- Modal footer -->
    	<div class="modal-footer">
      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    	</div>
  	</div>
</div>
</div>

<script>
	//Slider
$(document).ready(function(){
  	$('.slider').slick({
  		autoplay: true,
 		autoplaySpeed: 1000,
 		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 3
  	});
	
});
</script>



<?php include_once "../includes/rodape.php";?>