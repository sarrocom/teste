<?php 

require_once ("../includes/cabecalho.php");
require_once ("../includes/menu.php");
require_once ('../conexao.php');
require_once ('../sql/dashboard.php');

?>
<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Dashboard</h1>
			</div>
			<div class="col-md-4">
				<p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
				<a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
			</div>
		</div>
	</div>	
</div>
<div class="dashboard">
	<div class="container">
		<div class="icones">
			<div class="row">
				
				<div class="col-md-4 ">
					<div class="opcoesDashboard">
						<a href="pacientes.php">
							<i class="fas fa-users iconeAreas"></i>
							<h5>Pacientes em tratamento</h5>
							<p><strong><?=$totalPaciente?></strong></p>
						</a>
					</div>
				</div>

				<div class="col-md-4 ">
					<div class="opcoesDashboard">
						<a href="agenda.php">
							<i class="far fa-calendar-alt iconeAreas"></i>
							<h5>Agendamentos para essa semana</h5>
							<p><strong><?=$totalAgendamento?></strong></p>
						</a>
					</div>
				</div>

				<div class="col-md-4 ">
					<div class="opcoesDashboard">
						<a href="produtos.php">
							<i class="fas fa-prescription-bottle-alt iconeAreas"></i>
							<h5>Número de produtos cadastrados </h5>
							<p><strong><?=$totalProdutos?></strong></p>
						</a>
					</div>
				</div>

				<div class="col-md-4 ">
					<div class="opcoesDashboard">
						<a href="pacientes.php">
							<i class="fas fa-clipboard-check iconeAreas"></i>
							<h5>Última consulta</h5>
							<p><strong><?=$totalPaciente?></strong></p>
						</a>
					</div>
				</div>

				<div class="col-md-4 ">
					<div class="opcoesDashboard">
						<a href="suaconta.php?id=<?=$_SESSION['idMedico']
?>">
							<i class="fas fa-cogs iconeAreas"></i>
							<h5>Sua conta / como usar</h5>
							<!--<p><strong><?=$totalPaciente?></strong></p>-->
						</a>
					</div>
				</div>

				<div class="col-md-4 ">
					<div class="opcoesDashboard">
						<a href="enfermidades.php">
							<i class="fas fa-list-ul iconeAreas"></i>
							<h5>Lista de enfermidades</h5>
							<p><strong><?=$totalEnfermidade?></strong></p>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


<?php include_once "../includes/rodape.php";?>