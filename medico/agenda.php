<?php 
require_once( "../includes/cabecalho.php");
require_once( "../includes/menu.php");
require_once( '../conexao.php');
require_once('../sql/agendamento.php');
require_once('../sql/select.php');

$agendamentosDia = agendamentoDia($conexao);
$agendamentosMes = agendamentoMes($conexao);
$pacientes = exibeListaPaciente($conexao);

?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Sua agenda médica</h1>
			</div>
			<div class="col-md-4">
				<p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
				<a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
			</div>
		</div>
	</div>	
</div>

<div class="agenda">
	<div class="container">
	    <div class="row">

	        <div class="col-sm-12 col-md-9" style="margin: 50px 0;">
	        	<h3>Agenda do Mês</h3>
	                <hr>
	        	<div class="row">
	                
	                <?php 
	                    foreach ($agendamentosMes as $agendamentoMes) : 
	                    $dataD = explode(" ", $agendamentoMes['data']);
	                    $data = date('d/m/Y', strtotime($dataD[0])); 
	                    if ($_SESSION['nomeUsuario'] == $agendamentoMes['medico']) { ?>
	          	                 
	                <div class="col-md-3">
	                   <div class="" style="border:1px solid grey; padding: 10px;box-shadow: 1px 5px 13px 2px #b9b2b2;">
	                        <p><strong>Especialidade:</strong><br> <?=$agendamentoMes['especialidade']?></p>
	                        <p><strong>Médico:</strong><br> <?=$agendamentoMes['medico']?></p>
	                        <p><strong>Data:</strong><br> <?=$data?></p> 
	                        <p><strong>Horário:</strong><br> <?=$dataD[1]?></p> 
	                        <p><strong>Paciente:</strong><br> <?=$agendamentoMes['paciente']?></p>
	                        <p><strong>RG:</strong><br> <?=$agendamentoMes['rg']?></p>  
	                    </div>
	                </div>
	                <?php } endforeach ?>
	            </div>
            </div>
			
			<div id="overview" class="col-sm-4 col-md-3">
	            <div class="headerAgendaRight">
	            	<h4>Agenda do dia</h4>
	            </div>
	          
	            <div class="conteudoAgenda">
	            	<div class="row">
	            		<?php 
		                    foreach ($agendamentosDia as $agendamentoDia) : 
		                    $dataD = explode(" ", $agendamentoDia['data']);
		                    $data = date('d/m/Y', strtotime($dataD[0])); 

		                    if ($_SESSION['nomeUsuario'] == $agendamentoDia['medico']) { 
		                ?>               
						<div class="col-sm-12 consultaDoDia" style="border-bottom: 1px solid #e5e5e5;">
							<p class="consultaHorario"><b>Horário:</b> <?=$dataD[1]?></p>
							<p class="consultaPaciente"><b>Paciente:</b> <?=$agendamentoDia['paciente']?></p>
							<?php 

								foreach ($pacientes as $paciente){
									if ($agendamentoDia['rg'] == $paciente['rg']) { ?>
								
										<form action="consultas.php" method="post">
											<?php $_SESSION['cpf'] = $paciente['cpf']?>
											<input type="text" name="cpf" value="<?=$_SESSION['cpf']?>">
											<button class="btn btn-default">Ficha</button>
										</form>
							<?php 
									} 
								} 
							}?>
						
						</div>
						<?php endforeach ?>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
</div>


<?php require_once( "../includes/rodape.php");
include_once('../admin/includes/footer.php');
?>