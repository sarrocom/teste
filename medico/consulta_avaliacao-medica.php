<?php 
require_once("../includes/cabecalho.php");
require_once("../includes/menu.php");
require_once('../conexao.php');
require_once('../sql/select.php');
require_once('../sql/consulta.php');

$cpf = $_SESSION['cpf'];
$nome = $_SESSION['nome'];
$pacientes = exibeListaPaciente($conexao);
$enfermidade = exibeListaEnfermidade($conexao);
$avaliacaoMedica = exibeAvaliacaomedica($conexao);


?>

<div class="barraTitlePage" >
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Consultas</h1>
			</div>
			<div class="col-md-4">
                <p>Seja bem vindo, <?=$_SESSION['nomeUsuario']?>  
                <a style="position: relative;top: 4px;" href="../logout.php"><i class="fas fa-sign-out-alt" style="color:red; font-size: 20px;"></i></a></p>
            </div>
		</div>
	</div>	
</div>

<div id="consulta">
	<div class="container">
		<div class="row">
			<div class="col-md-3 aside" >

				<div class="outrasOpcoes">
					<a href="consultas.php" class="btn botoesDefault ">Anotações</a>
					<a href="consulta_avaliacao-medica.php" class="btn botoesDefault anotacoes">Avaliação médica</a>
					<a href="consulta_produtos.php" class="btn botoesDefault" >Selecione Produtos</a>
					<a href="consulta_posologia.php" class="btn botoesDefault">Posologia</a>
					<a href="consulta_prescricao.php" class="btn botoesDefault">Prescrição</a>
					<a href="consulta_laudo-medico.php" class="btn botoesDefault">Laudo médico</a>
				</div>
				
			</div>
				
			<div class="col-md-9 main" >
				<?php 
				if (isset($_SESSION['msg'])) {
					echo $_SESSION['msg'];
					unset ($_SESSION['msg']);
				}
				?>				
				<div class="col-sm-12 col-md-12 avaliacaoMedica">
					<div class="row">
						<div class="col-md-12">
							<h6 class="tituloSeccao">Avaliação médica</h6>
						</div>
					</div>
					<form action="../recebe-forms/recebe-avaliacaomedica.php" method="post">							
						<div class="areaAvaliacao">
							<div class="row">
								<div class="col-md-12">
									<div class="selectEnfermidades">
										<div class="form-group">
											<select name="enfermidade" id="" class="enfermidade">
												<?php
			                                    $enfermidades = exibeListaEnfermidade($conexao);
			                                    foreach ($enfermidades as $enfermidade) :
			                                    ?>
			                                    <option value="<?=$enfermidade['nome_enfermidade']?>"><?=$enfermidade['nome_enfermidade']?></option>  
			                                    <?php endforeach ?> 
											</select>
										</div>	
									</div>
									<?php foreach ($avaliacaoMedica as $avaliacaoMedica): ?>
										<div class="form-group descricao">
											<label for="descricao">Descrição do caso</label>
											<textarea name="descricao" class="form-control" cols="30" rows="10" ><?=$avaliacaoMedica['descricao_caso']?></textarea>
										</div>
										<div class="form-group descricao">
											<label for="tratamentos">Tratamentos anteriores</label>
											<textarea name="tratamentos" class="form-control" id="" cols="30" rows="10"><?=$avaliacaoMedica['tratamento']?></textarea>
										</div>

									<?php endforeach ?>									
								</div>
							</div>
							<?php foreach ($pacientes as $paciente) : ?>
								<?php if ($cpf == $paciente['cpf']): ?>
									<input type="hidden" name="nome" value="<?=$paciente['nome']?>">
									<input type="hidden" name="cpf" value="<?=$paciente['cpf']?>">
								<?php endif ?>
								
							<?php endforeach ?>

						</div>
						<div class="form-group botaoSalvar">
															
							<button type="submit" class="btn botoesDefault">Salvar</button>
							
						</div>
					</form>
				</div>

			</div>

			
		</div>

	</div>
</div>



<?php require_once("../includes/rodape.php");?>