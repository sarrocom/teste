function valida_formulario_medico(){
    
    var nome = formularioMedico.nome.value;
    var crm = formularioMedico.crm.value;
    var email = formularioMedico.email.value;
    var cep = formularioMedico.cep.value;
    var endereco = formularioMedico.rua.value;
    var numero = formularioMedico.numero.value;
    var cidade = formularioMedico.cidade.value;
    var uf = formularioMedico.uf.value;
    var cpf = formularioMedico.cpf.value;
    var telefone = formularioMedico.telefone.value;


    if (nome == '') {
        document.getElementById("nomeC").innerHTML = 'Preencha o NOME';
        formularioMedico.nome.focus();
        formularioMedico.nome.classList.add("erro");
        return false;
    }

    if (crm == '') {
        document.getElementById("crM").innerHTML = 'Preencha o CRM correto';
        formularioMedico.crm.focus();
        formularioMedico.crm.classList.add("erro");
        return false;
    }

    if (email == '') {
        document.getElementById("emaiL").innerHTML = 'Preencha o E-MAIL correto';
        formularioMedico.email.focus();
        formularioMedico.email.classList.add("erro");
        return false;
    }

    if (cep == '') {
        document.getElementById("ceP").innerHTML = 'Preencha o CEP correto';
        formularioMedico.cep.focus();
        formularioMedico.cep.classList.add("erro");
        return false;
    }
    if (endereco == '') {
        document.getElementById("enderecO").innerHTML = 'Preencha o endereco correto';
        formularioMedico.endereco.focus();
        formularioMedico.endereco.classList.add("erro");
        return false;
    }
   
    if (numero == '') {
        document.getElementById("numerO").innerHTML = 'Preencha o NUMERO correto';
        formularioMedico.numero.focus();
        formularioMedico.numero.classList.add("erro");
        return false;
    }

    if (cidade == '') {
        document.getElementById("cidadE").innerHTML = 'Preencha o cidade correto';
        formularioMedico.cidade.focus();
        formularioMedico.cidade.classList.add("erro");
        return false;
    }

    if (uf == '') {
        document.getElementById("uF").innerHTML = 'Preencha o uf correto';
        formularioMedico.uf.focus();
        formularioMedico.uf.classList.add("erro");
        return false;
    }
    
    
    if (cpf == '') {
        document.getElementById("cpF").innerHTML = 'Preencha o CPF correto';
        formularioMedico.cpf.focus();
        formularioMedico.cpf.classList.add("erro");
        return false;
    }
     
    if (telefone == '') {
        document.getElementById("telefonE").innerHTML = 'Preencha o TELEFONE correto';
        formularioMedico.telefone.focus();
        formularioMedico.telefone.classList.add("erro");
        return false;
    }
   
}


function valida_formulario_paciente(){
    
    var nome = formularioPaciente.nome.value;
    var email = formularioPaciente.email.value;
    var cpf = formularioPaciente.cpf.value;
    var nascimento = formularioPaciente.nascimento.value;
    var rg = formularioPaciente.rg.value;


     if (nome == '') {
        document.getElementById("nomeC").innerHTML = 'Preencha o NOME';
        formularioPaciente.nome.focus();
        formularioPaciente.nome.classList.add("erro");
        return false;
    }

    if (cpf == '') {
        document.getElementById("cpF").innerHTML = 'Preencha o CPF correto';
        formularioPaciente.cpf.focus();
        formularioPaciente.cpf.classList.add("erro");
        return false;
    }

    if (email == '') {
        document.getElementById("emaiL").innerHTML = 'Preencha o E-MAIL correto';
        formularioPaciente.email.focus();
        formularioPaciente.email.classList.add("erro");
        return false;
    }

    if (nascimento == '') {
        document.getElementById("nascimentO").innerHTML = 'Preencha o campo NASCIMENTO';
        formularioPaciente.nascimento.focus();
        formularioPaciente.nascimento.classList.add("erro");
        return false;
    }

    if (rg == '') {
        document.getElementById("RG").innerHTML = 'Preencha o campo RG';
        formularioPaciente.rg.focus();
        formularioPaciente.rg.classList.add("erro");
        return false;
    }

    if (telefone == '') {
        document.getElementById("telefonE").innerHTML = 'Preencha o TELEFONE correto';
        formularioMedico.telefone.focus();
        formularioMedico.telefone.classList.add("erro");
        return false;
    }

}

function valida_formulario_produto(){
    
    var nome = formularioProduto.nome.value;
    var descricao = formularioProduto.descricao.value;
    var posologia = formularioProduto.posologia.value;
    var preco = formularioProduto.preco.value;
    //var enfermidade = formularioProduto.enfermidades.value;
    //var foto = formularioProduto.foto.value;


     if (nome == '') {
        document.getElementById("produtO").innerHTML = 'Preencha o NOME';
        formularioProduto.nome.focus();
        formularioProduto.nome.classList.add("erro");
        return false;
    }

    if (descricao == '') {
        document.getElementById("descricaO").innerHTML = 'Preencha o E-MAIL correto';
        formularioProduto.descricao.focus();
        formularioProduto.descricao.classList.add("erro");
        return false;
    }

    if (posologia == '') {
        document.getElementById("posologiA").innerHTML = 'Preencha o posologia correto';
        formularioProduto.posologia.focus();
        formularioProduto.posologia.classList.add("erro");
        return false;
    }

    if (preco == '') {
        document.getElementById("precO").innerHTML = 'Preencha o PREÇO correto';
        formularioProduto.preco.focus();
        formularioProduto.preco.classList.add("erro");
        return false;
    }
/*
    if (enfermidade == '') {
        document.getElementById("enfermidadE").innerHTML = 'Preencha o campo enfermidade';
        formularioProduto.enfermidade.focus();
        formularioProduto.enfermidade.classList.add("erro");
        return false;
    }

    if (foto == '') {
        document.getElementById("fotO").innerHTML = 'Preencha o campo foto';
        formularioProduto.foto.focus();
        formularioProduto.foto.classList.add("erro");
        return false;
    }

*/
}