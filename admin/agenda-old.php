<?php 
    session_start();
    require_once('includes/header.php'); 
    require_once('includes/menu.php');
    require_once('../conexao.php');
    require_once('../sql/select.php');

    $result_events = "SELECT * FROM events";
    $resultado_events =mysqli_query($conexao, $result_events);


?>

<script>

  $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: Date(),
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            eventClick: function(event) {
                
                $('#visualizar #id').text(event.id);
                $('#visualizar #title').text(event.title);
                $('#visualizar #start').text(event.start.format('DD/MM/YYYY HH:mm:ss'));
                $('#visualizar #end').text(event.end.format('DD/MM/YYYY HH:mm:ss'));
                $('#visualizar #especialidade').text(event.especialidade);

                $('#visualizar').modal('show');
                return false;

            },
            
            selectable: true,
            selectHelper: true,
            select: function(start, end){
                $('#cadastrar #start').val(moment(start).format('DD/MM/YYYY HH:mm:ss'));
                $('#cadastrar #end').val(moment(end).format('DD/MM/YYYY HH:mm:ss'));
                $('#cadastrar').modal('show');                      
            },
            events: [
                <?php
                    while($row_events = mysqli_fetch_array($resultado_events)){

                        ?>
                        {
                        id_agendamento: '<?php echo $row_events['id']; ?>',
                        title: '<?php echo $row_events['title']; ?>',
                        start: '<?php echo $row_events['start']; ?>',
                        end: '<?php echo $row_events['end']; ?>',
                        color: '<?php echo $row_events['color']; ?>',
                        especialidade: '<?php echo $row_events['especialidade']; ?>',
                        
                        },<?php
                    }
                ?>
            ]
        });
    });
    
    //Mascara para o campo data e hora
    function DataHora(evento, objeto){
        var keypress=(window.event)?event.keyCode:evento.which;
        campo = eval (objeto);
        if (campo.value == '00/00/0000 00:00:00'){
            campo.value=""
        }
     
        caracteres = '0123456789';
        separacao1 = '/';
        separacao2 = ' ';
        separacao3 = ':';
        conjunto1 = 2;
        conjunto2 = 5;
        conjunto3 = 10;
        conjunto4 = 13;
        conjunto5 = 16;
        if ((caracteres.search(String.fromCharCode (keypress))!=-1) && campo.value.length < (19)){
            if (campo.value.length == conjunto1 )
            campo.value = campo.value + separacao1;
            else if (campo.value.length == conjunto2)
            campo.value = campo.value + separacao1;
            else if (campo.value.length == conjunto3)
            campo.value = campo.value + separacao2;
            else if (campo.value.length == conjunto4)
            campo.value = campo.value + separacao3;
            else if (campo.value.length == conjunto5)
            campo.value = campo.value + separacao3;
        }else{
            event.returnValue = false;
        }
    }
</script>

<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Agenda</h2>
                <?php
                if(isset($_SESSION['msg'])){
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
                ?>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">                
                <div id='calendar'></div>  
            </div>
            <div class="modal fade" id="visualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center">Dados do Evento</h4>
                        </div>
                        <div class="modal-body">
                            <dl class="dl-horizontal">
                                <dt>ID do Evento</dt>
                                <dd id="id"></dd>
                                <dt>Titulo do Evento</dt>
                                <dd id="title"></dd>
                                <dt>Inicio do Evento</dt>
                                <dd id="start"></dd>
                                <dt>Fim do Evento</dt>
                                <dd id="end"></dd>
                                <dt>Especialidade</dt>
                                <dd id="especialidade"></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center">Cadastrar Consulta</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="POST" action="proc_cad_evento.php">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="teste">Especialidade</label>
                                        <select class="form-control" name="especialidade" style="height: 50px;">
                                            <option>Selecione uma especialidade</option>
                                            <?php
                                            $especialidades = exibeListaEspecialidade($conexao);
                                            foreach ($especialidades as $especialidade) :
                                            ?>
                                            <option value="<?=$especialidade['especialidade']?>"><?=$especialidade['especialidade']?></option>
                                            <?php endforeach ?>
                                        </select>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="teste">Médico</label>
                                        <select class="form-control" name="medicoAgendamento" style="height: 50px;">
                                            <option>Selecione um médico</option>
                                            <?php
                                            $medicos = exibeListaMedicos($conexao);
                                            foreach ($medicos as $medico) :
                                            ?>
                                            <option value="<?=$medico['nome']?>"><?=$medico['nome']?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="">Data</label>
                                        <input type="text" class="form-control" name="start" id="start" onKeyPress="DataHora(event, this)">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="teste">Horários</label>
                                        <select class="form-control" name="horarioAgendamento" style="height: 50px;">
                                            <option>Selecione um Horário</option>
                                            <option value="10:00_a_11:00">10:00 a 11:00</option>
                                            <option value="11:00_a_12:00">11:00 a 12:00</option>
                                            <option value="12:00_a_13:00">12:00 a 13:00</option>
                                            <option value="13:00_a_14:00">13:00 a 14:00</option>
                                            <option value="14:00_a_15:00">14:00 a 15:00</option>
                                        </select>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" >Nome Paciente</label>
                                        <input type="text" class="form-control" name="pacienteAgendamento" placeholder="Nome Paciente">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label for="">Idade</label>
                                        <input type="text" class="form-control" name="idadeAgendamento" placeholder="00/00/0000">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="teste">Sexo</label>
                                        <select class="form-control" name="sexoAgendamento" style="height: 50px;">
                                            <option>Masculino</option>
                                            <option>Feminino</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="teste">RG</label>
                                        <input type="text" class="form-control" name="rgAgendamento" placeholder="RG">
                                    </div>
                                </div>

                                <!--
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Cor</label>
                                    <div class="col-sm-10">
                                        <select name="color" class="form-control" id="color">
                                            <option value="">Selecione</option>         
                                            <option style="color:#FFD700;" value="#FFD700">Amarelo</option>
                                            <option style="color:#0071c5;" value="#0071c5">Azul Turquesa</option>
                                            <option style="color:#FF4500;" value="#FF4500">Laranja</option>
                                            <option style="color:#8B4513;" value="#8B4513">Marrom</option>  
                                            <option style="color:#1C1C1C;" value="#1C1C1C">Preto</option>
                                            <option style="color:#436EEE;" value="#436EEE">Royal Blue</option>
                                            <option style="color:#A020F0;" value="#A020F0">Roxo</option>
                                            <option style="color:#40E0D0;" value="#40E0D0">Turquesa</option>                                        
                                            <option style="color:#228B22;" value="#228B22">Verde</option>
                                            <option style="color:#8B0000;" value="#8B0000">Vermelho</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Data Inicial</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="start" id="start" onKeyPress="DataHora(event, this)">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Data Final</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="end" id="end" onKeyPress="DataHora(event, this)">
                                    </div>
                                </div>
                               -->
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Cadastrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->


<?php include_once('includes/footer.php');?>