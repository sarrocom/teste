<?php 
    session_start();
    require_once('includes/header.php'); 
    require_once('includes/menu.php');
    require_once('../conexao.php');
    require_once('../sql/select.php');

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Cadastro Produto </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-9">
               <?php 
               if(isset($_SESSION['sucesso'])) {  
                    echo $_SESSION['sucesso'];  
                    unset($_SESSION['sucesso']);
                } 
                if(isset($_SESSION['erro'])) { 
                    echo $_SESSION['erro'];
                    unset($_SESSION['erro']);
                } 
                ?>
                
                <form name="formularioProduto" method="post" action="../recebe-forms/recebe-formularioProduto.php" enctype="multipart/form-data">
                    <div class="col-lg-9" >
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeProduto" id="nome" class="form-control" placeholder="NOME DO PRODUTO">
                                <div id="produtO"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="nomePrincipioAtivo" id="nomePrincipioAtivo" class="form-control" placeholder="NOME PRINCIPIO ATIVO">
                                <div id="nomePrincipioAtivo"></div>
                            </div>  
                            <div class="form-group">
                                <textarea class="form-control" name="descricao" id="descricao" cols="30" rows="10" placeholder="DESCRIÇÃO"></textarea>
                                <div id="descricaO"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeMarca" id="nomeMarca" class="form-control" placeholder="NOME DA MARCA">
                                <div id="nomeMarca"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="preco" id="preco" class="form-control" placeholder="PREÇO SUGERIDO">
                                <div id="precO"></div>
                            </div>
                            
                            <div class="form-group">

                                <label for="enfermidades">Enfermidades</label>
                                <select multiple="multiple" class="form-control select" name="enfermidades[]" style="height: 170px;">
                                    <?php
                                    $enfermidades = exibeListaEnfermidade($conexao);
                                    foreach ($enfermidades as $enfermidade) :
                                    ?>
                                    <option value="<?=$enfermidade['nome_enfermidade']?>"><?=$enfermidade['nome_enfermidade']?></option>  
                                    <?php endforeach ?>            
                                </select>
                                <span class="span"></apan>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <h4 class="page-header">Posologia</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="form-group">
                                    <input type="text" name="quantidade_posologia" id="quantidade" class="form-control" placeholder="QUANTIDADE">
                                    <div id="quantidadE"></div>
                                </div>
                               
                                <div class="form-group">
                                    <input type="number" name="quantidadeFrascos_posologia" id="quantidadeFrascos" class="form-control" placeholder="QUANTIDADE DE FRESCOS">
                                    <div id="quantidadEFrascos"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                             <div class="form-group">
                                <select name="tipoDeIngestao_posologia" class="form-control" style="height: 50px;">
                                    <option value="gotas">Gotas</option>
                                    <option value="miligramas">Miligramas</option>
                                    <option value="centimetros">Centimetros</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="number" name="periodicidade_posologia" id="periodicidade" class="form-control" placeholder="Periodicidade">
                                <div id="periodicidade"></div>
                            </div>
                        </div>  

                        <div class="col-lg-12">
                            <h4 class="page-header">Fabricante</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeFabricante" id="nomeFabricante" class="form-control" placeholder="nomeFabricante">
                                <div id="quantidadE"></div>
                            </div>
                           
                            <div class="form-group">
                                 <input class="form-control" name="ruaFabricante" type="text" id="rua" placeholder="ENDEREÇO"/>
                                <div id="enderecO"></div>
                            </div>

                            <div class="form-group">
                                 <input class="form-control" name="cidadeFabricante" type="text" id="cidade" size="40" placeholder="CIDADE"/>
                                <div id="cidadE"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input class="form-control" name="cepFabricante" type="text" id="cep" value="" size="10" maxlength="9" placeholder="CEP" />
                                <div id="ceP"></div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                 <div class="col-lg-4" style="padding-right: 0">
                                    <input class="form-control" name="numeroFabricante" id="numero" placeholder="Nº">
                                    <div id="numerO"></div>
                                 </div>
                                 <div class="col-lg-8">
                                     <input class="form-control" name="complementoFabricante" type="text" id="complemento" value="" placeholder="COMPLEMENTO">
                                 </div>
                                 </div>
                            </div>
                            <div class="form-group">
                                 <input class="form-control" name="ufFabricante" type="text" id="uf" size="2" placeholder="ESTADO"/>
                                <div id="estadO"></div>
                            </div>
                        </div>  
                       
                       
                        <div class="col-lg-12">
                            <h4 class="page-header">Exportador</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeExportador" id="nomeFabricante" class="form-control" placeholder="NOME EXPORTADOR">
                                <div id="quantidadE"></div>
                            </div>
                           
                            <div class="form-group">
                                 <input class="form-control" name="ruaExportador" type="text" id="ruaExportador" placeholder="ENDEREÇO"/>
                                <div id="enderecO"></div>
                            </div>

                            <div class="form-group">
                                 <input class="form-control" name="cidadeExportador" type="text" id="cidadeExportador" size="40" placeholder="CIDADE"/>
                                <div id="cidadE"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input class="form-control" name="cepExportador" type="text" id="cepExportador" value="" size="10" maxlength="9" placeholder="CEP" />
                                <div id="ceP"></div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                     <div class="col-lg-4" style="padding-right: 0">
                                        <input class="form-control" name="numeroExportador" id="numero" placeholder="Nº">
                                        <div id="numerO"></div>
                                     </div>
                                     <div class="col-lg-8">
                                         <input class="form-control" name="complementoExportador" type="text" id="complementoExportador" value="" placeholder="COMPLEMENTO">
                                     </div>
                                </div>
                            </div>
                            <div class="form-group">
                                 <input class="form-control" name="ufExportador" type="text" id="ufExportador" size="2" placeholder="ESTADO"/>
                                <div id="estadO"></div>
                            </div>
                        </div>
 
                    </div>

                    <div class="col-lg-3" style="margin-top: 50px;">
                        <div class="form-group">
                            <input type="file" name="fotoProduto" id="foto" class="form-control" placeholder="Selecione Sua foto">
                            <div id="fotO"></div>
                        </div>  
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group" style="text-align: center;margin-top: 30px;">
                            <button class="btn btn-primary" onclick="return valida_formulario_produto()">Cadastrar</button>
                        </div>
                    </div> 
                </form>
                
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
    <!-- /#page-wrapper -->
    <script>

        $( ".select" )
          .change(function() {
            var str = "";
            $( ".select option:selected" ).each(function() {
              str += $( this ).text() + " ";
            });
            $( ".span" ).text( str );
        })
        .trigger( "change" );

    </script>

<?php include_once('includes/footer.php');?>