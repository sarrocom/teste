<?php 
    session_start();
    include_once('includes/header.php'); 
    include_once('includes/menu.php');

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Cadastro Enfermidade </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-9">
               <?php 
                    if(isset($_SESSION['msg'])) {                     
                        echo  $_SESSION['msg'];
                        unset($_SESSION['msg']);
                    } 
                ?>
                                
                <form name="formularioEnfermidade" method="post" action="../recebe-forms/recebe-formularioenfermidade.php" >
                    <div class="col-lg-9" >

                        <div class="form-group">
                            <input type="text" name="enfermidade" class="form-control" placeholder="NOME DA ENFERMIDADE">
                            <div id="enfermidade"></div>
                       </div>

                        <div class="form-group" style="text-align: center;margin-top: 30px;">
                            <button class="btn btn-primary" onclick="return valida_formulario_produto()">Cadastrar</button>
                        </div>
                    </div>   
                    
                </form>
                
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
    <!-- /#page-wrapper -->
    

<?php include_once('includes/footer.php');?>