<?php 
    session_start();
    require_once('includes/header.php'); 
    require_once('includes/menu.php');
    require_once('../conexao.php');
    require_once('../sql/select.php');
    require_once('../sql/agendamento.php');



$agendamentosDia = agendamentoDia($conexao);
$agendamentosMes = agendamentoMes($conexao);


?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Agenda</h2>
                <?php
                if(isset($_SESSION['msg'])){
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
                ?>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">                
                <button class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                Agendamento
                </button>   
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Agendamento de consultas</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           <form class="form-horizontal" method="POST" action="../recebe-forms/agendamento.php"">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="teste">Especialidade</label>
                                        <select class="form-control" name="especialidade" style="height: 50px;">
                                            <option>Selecione uma especialidade</option>
                                            <?php
                                            $especialidades = exibeListaEspecialidade($conexao);
                                            foreach ($especialidades as $especialidade) :
                                            ?>
                                            <option value="<?=$especialidade['especialidade']?>"><?=$especialidade['especialidade']?></option>
                                            <?php endforeach ?>
                                        </select>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="teste">Médico</label>
                                        <select class="form-control" name="medicoAgendamento" style="height: 50px;">
                                            <option>Selecione um médico</option>
                                            <?php
                                            $medicos = exibeListaMedicos($conexao);
                                            foreach ($medicos as $medico) :
                                            ?>
                                            <option value="<?=$medico['nome']?>"><?=$medico['nome']?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="">Data</label>
                                        <div class="input-group date data_formato" data-date-format="dd/mm/yyyy HH:ii:ss">
                                            <input type="text" class="form-control" name="data" placeholder="Data da visita">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </span>
                                        </div> 
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputEmail3" >Nome Paciente</label>
                                        <input type="text" class="form-control" name="pacienteAgendamento" placeholder="Nome Paciente">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label for="">Idade</label>
                                        <input type="text" class="form-control" name="idadeAgendamento" placeholder="00/00/0000">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="teste">Sexo</label>
                                        <select class="form-control" name="sexoAgendamento" style="height: 50px;">
                                            <option>Masculino</option>
                                            <option>Feminino</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="teste">RG</label>
                                        <input type="text" class="form-control" name="rgAgendamento" placeholder="RG">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Cadastrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                </div>  
            </div>

            <div class="col-md-12" style="margin: 50px 0;">
                <h3>Agendamentos do dia</h3>
                <hr>
                <hr>
                <?php 
                    foreach ($agendamentosDia as $agendamentoDia) : 
                    $dataD = explode(" ", $agendamentoDia['data']);
                    $data = date('d/m/Y', strtotime($dataD[0]));                       
                ?>
                   
                <div class="col-md-3">
                   <div class="" style="border:1px solid grey; padding: 10px;box-shadow: 1px 5px 13px 2px #b9b2b2;">
                        <p><strong>Especialidade:</strong><br> <?=$agendamentoDia['especialidade']?></p>
                        <p><strong>Médico:</strong><br> <?=$agendamentoDia['medico']?></p>
                        <p><strong>Data:</strong><br> <?=$data?></p> 
                        <p><strong>Horário:</strong><br> <?=$dataD[1]?></p> 
                        <p><strong>Paciente:</strong><br> <?=$agendamentoDia['paciente']?></p>
                        <p><strong>RG:</strong><br> <?=$agendamentoDia['rg']?></p>  
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <div class="col-md-12" style="margin: 50px 0;">
                <h3>Agendamentos do Mês</h3>
                <hr>
                <?php 
                    foreach ($agendamentosMes as $agendamentoMes) : 
                    $dataD = explode(" ", $agendamentoMes['data']);
                    $data = date('d/m/Y', strtotime($dataD[0]));                       
                ?>
                   
                <div class="col-md-3">
                   <div class="" style="border:1px solid grey; padding: 10px;box-shadow: 1px 5px 13px 2px #b9b2b2;">
                        <p><strong>Especialidade:</strong><br> <?=$agendamentoMes['especialidade']?></p>
                        <p><strong>Médico:</strong><br> <?=$agendamentoMes['medico']?></p>
                        <p><strong>Data:</strong><br> <?=$data?></p> 
                        <p><strong>Horário:</strong><br> <?=$dataD[1]?></p> 
                        <p><strong>Paciente:</strong><br> <?=$agendamentoMes['paciente']?></p>
                        <p><strong>RG:</strong><br> <?=$agendamentoMes['rg']?></p>  
                    </div>
                </div>
                <?php endforeach ?>

            </div>
            
        </div>
    </div>
    <!-- /#page-wrapper -->


<?php include_once('includes/footer.php');?>