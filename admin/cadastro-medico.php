<?php 
    session_start();
    require_once('includes/header.php'); 
    require_once('includes/menu.php');
    require_once('../conexao.php');
    require_once('../sql/select.php');

?>
<style>


</style>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Cadastro médico </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-9">

                <?php if(isset($_SESSION['sucesso'])) { ?>
                    <div class="alert alert-success" role="alert">
                        <strong><?php echo $_SESSION['sucesso'];?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>        
                <?php 
                    unset($_SESSION['sucesso']);
                } ?>
                <?php if(isset($_SESSION['erro'])) { ?>
                    <div class="alert alert-danger" role="alert">
                        <strong><?php echo $_SESSION['erro'];?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>        
                <?php 
                    unset($_SESSION['erro']);
                } ?>

                <form name="formularioMedico" method="post" action="../recebe-forms/recebe-formularioMedico.php">
                    <div class="form-group">
                        <input class="form-control" name="nome" id="nome" placeholder="NOME COMPLETO">
                        <div id="nomeC"></div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="crm" id="crm" placeholder="CRM">
                        <div id="crM"></div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <input class="form-control" name="email" id="email" placeholder="E-MAIL"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                                <div id="emaiL"></div>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="senha" id="senha" placeholder="SENHA" value="sistemaAnell@2018">
                                <div id="emaiL"></div>
                            </div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="cep" type="text" id="cep" value="" size="10" maxlength="9" placeholder="CEP" />
                        <div id="ceP"></div>
                    </div>
                    <div class="form-group" style="overflow: hidden;">
                        <div class="row">
                            <div class="col-lg-4">
                                <input class="form-control" name="rua" type="text" id="rua" placeholder="ENDEREÇO"/>
                                <div id="enderecO"></div>
                            </div>
                            <div class="col-lg-4">
                                <input class="form-control" name="numero" id="numero" placeholder="NUMERO">
                                <div id="numerO"></div>
                            </div>
                            <div class="col-lg-4">
                                <input class="form-control" name="complemento" type="text" id="complemento" value="" placeholder="COMPLEMENTO">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group" style="overflow: hidden;">
                        <div class="row"> 
                            <div class="col-lg-6">
                                <input class="form-control" name="cidade" type="text" id="cidade" size="40" placeholder="CIDADE"/>
                                <div id="cidadE"></div>
                            </div>
                            <div class="col-lg-6">
                                <input class="form-control" name="uf" type="text" id="uf" size="2" placeholder="ESTADO"/>
                                <div id="estadO"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input class="form-control" name="cpf" id="cpf" placeholder="CPF">
                        <div id="cpF"></div>
                    </div>

                    <div class="form-group">
                        <input class="form-control" name="telefone" id="telefone" placeholder="TELEFONE">
                        <div id="telefonE"></div>
                    </div>

                    <div class="form-group" style="overflow: hidden;">
                        <div class="row">
                            <div class="col-lg-6">
                                <select class="form-control" name="areaAtuacao" style="height: 50px;">
                                    <option>Selecione uma especialidade</option>
                                    <?php
                                    $especialidades = exibeListaEspecialidade($conexao);
                                    foreach ($especialidades as $especialidade) :
                                    ?>
                                    <option value="<?=$especialidade['especialidade']?>"><?=$especialidade['especialidade']?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                *Lista de especialidades
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 50px;">
                        <h5 class="text-center"><strong>Selecione os horários de trabalho da semana</strong></h5>
                        <table class="table table-bordered">
                            <thead>
                                <th>Dia</th>
                                <th>Hora chegada</th>
                                <th>Hora Saída</th>
                            </thead>
                            <tr>
                                <td>Segunda</td>
                                 <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioSegunda" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimSegunda" placeholder="Hora Fim">
                                </td>
                            </tr>
                            <tr>
                                <td>Terça</td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioTerca" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimTerca" placeholder="Hora Fim">
                                </td>
                            </tr>
                            <tr>
                                <td>Quarta</td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioQuarta" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimQuarta" placeholder="Hora Fim">
                                </td>
                            </tr>
                            <tr>
                                <td>Quinta</td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioQuinta" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimQuinta" placeholder="Hora Fim">
                                </td>
                            </tr>
                            <tr>
                                <td>Sexta</td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioSexta" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimSexta" placeholder="Hora Fim">
                                </td>
                            </tr>
                            <tr>
                                <td>Sábado</td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioSabado" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimSabado" placeholder="Hora Fim">
                                </td>
                            </tr>
                            <tr>
                                <td>Domingo</td>
                                 <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaInicioDomingo" placeholder="Hora ínicio">
                                </td>
                                <td style="width: 150px;">
                                     <input class="form-control chegada_saida" type="text" name="horaFimDomingo" placeholder="Hora Fim">
                                </td>
                            </tr>
                        </table>                            
                    </div>


                    <div class="form-group" style="text-align: center;">
                        <button class="btn btn-primary" onclick="return valida_formulario_medico()">Cadastrar</button>
                    </div>
                </form>

            </div>
            <div class="col-lg-3"></div>
        </div>
        
    </div>
    <!-- /#page-wrapper -->

<?php require_once('includes/footer.php');?>