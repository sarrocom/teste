<?php 
    session_start();
    include_once('includes/header.php'); 
    include_once('includes/menu.php');
    include_once('../conexao.php');
    include_once('../sql/select.php');

$pacientes = exibeListaPaciente($conexao);

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Lista de Pacientes </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading" style="overflow: hidden;">
                           <form action="">
                                <div class="col-lg-4">
                                   <div class="form-group input-group">
                                        <input type="text" class="form-control" placeholder="Buscar">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                   <div class="form-group input-group">
                                        <input type="text" class="form-control" placeholder="CPF">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                           </form>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php if(isset($_SESSION['sucesso'])) { ?>
                                <div class="alert alert-success" role="alert">
                                    <strong><?php echo $_SESSION['sucesso'];?></strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>        
                            <?php 
                                unset($_SESSION['sucesso']);
                            } ?>
                            <?php if(isset($_SESSION['erro'])) { ?>
                                <div class="alert alert-danger" role="alert">
                                    <strong><?php echo $_SESSION['erro'];?></strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>        
                            <?php 
                                unset($_SESSION['erro']);
                            } ?>
                            <table cellpadding="0"  class="table table-striped table-bordered" id="example">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Data Nascimento</th>
                                        <th>Sexo</th>
                                        <th>E-mail</th>
                                        <th>Edita</th>
                                        <th>Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php 
                                    foreach ($pacientes as $paciente) :
                                ?>    
                                    <tr class="even gradeA">
                                        <td><?=$paciente['id']?></td>
                                        <td><?=$paciente['nome']?></td>
                                        <td><?=$paciente['data_nascimento']?></td>
                                        <td><?=$paciente['sexo']?></td>
                                        <td><?=$paciente['email']?></td>
                                        <td class="center" style="width: 50px;text-align: center">
                                            <form action="altera-paciente.php" method="post">
                                                <input type="hidden" id="id" name="id" value="<?=$paciente['id']?>">
                                                <button type="submit" class="btn btn-primary btn-circle"><i class="fa fa-list"></i></button>
                                            </form>
                                        </td>
                                        <td class="center" style="width: 50px;text-align: center">
                                            <form action="../recebe-forms/removePaciente.php" method="post">
                                                <input type="hidden" id="id" name="id" value="<?=$paciente['id']?>">
                                                <button type="submit" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>    
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>            
        </div>
        
    </div>
    <!-- /#page-wrapper -->

<?php include_once('includes/footer.php');?>