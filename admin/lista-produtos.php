<?php 

    session_start();
    include_once('includes/header.php'); 
    include_once('includes/menu.php');
    include_once('../conexao.php');
    include_once('../sql/select.php');


$produtos = exibeListaProdutos($conexao);

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Lista de Produdos </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading" style="overflow: hidden;">
                           <form action="">
                                <div class="col-lg-4">
                                   <div class="form-group input-group">
                                        <input type="text" class="form-control" placeholder="BUSCA">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                   <div class="form-group input-group">
                                        <input type="text" class="form-control" placeholder="PALAVRA-CHAVE">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                           </form>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php 
                               if(isset($_SESSION['sucesso'])) {  
                                    echo $_SESSION['sucesso'];  
                                    unset($_SESSION['sucesso']);
                                } 
                                if(isset($_SESSION['erro'])) { 
                                    echo $_SESSION['erro'];
                                    unset($_SESSION['erro']);
                                } 
                            ?>
                            <table cellpadding="0"  class="table table-striped table-bordered" id="example">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Preço sugerido</th>
                                        <th>Edita</th>
                                        <th>Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    foreach ($produtos as $produto) :
                                ?>
                                    <tr class="even gradeA">
                                        <td><?=$produto['id']?></td>
                                        <td><?=$produto['nome_produto']?></td>
                                        <td><?=$produto['preco_sugerido']?></td>
                                        <td class="center" style="width: 50px;text-align: center">
                                            <form action="altera-produto.php" method="post">
                                                <input type="hidden" id="id" name="id" value="<?=$produto['id']?>">
                                                <button type="submit" class="btn btn-primary btn-circle"><i class="fa fa-list"></i></button>
                                            </form>
                                        </td>
                                        <td class="center" style="width: 50px;text-align: center">
                                            <form action="../recebe-forms/removeProduto.php" method="post">
                                                <input type="hidden" id="id" name="id" value="<?=$produto['id']?>">
                                                <button type="submit" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>    
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>            
        </div>
        
    </div>
    <!-- /#page-wrapper -->

<?php include_once('includes/footer.php');?>