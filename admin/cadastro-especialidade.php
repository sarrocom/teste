<?php 
    session_start();
    include_once('includes/header.php'); 
    include_once('includes/menu.php');

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Cadastro Especialidade </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-9">
               <?php if(isset($_SESSION['sucesso'])) { ?>
                    <div class="alert alert-success" role="alert">
                        <strong><?php echo $_SESSION['sucesso'];?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>        
                <?php 
                    unset($_SESSION['sucesso']);
                } ?>
                <?php if(isset($_SESSION['erro'])) { ?>
                    <div class="alert alert-danger" role="alert">
                        <strong><?php echo $_SESSION['erro'];?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>        
                <?php 
                    unset($_SESSION['erro']);
                } ?>
                
                <form name="formularioEspecialidade" method="post" action="../recebe-forms/recebe-formularioespecialidade.php" >
                    <div class="col-lg-9" >

                        <div class="form-group">
                            <input type="text" name="especialidade" class="form-control" placeholder="NOME DA ESPECIALIDADE">
                            <div id="especialidade"></div>
                       </div>

                        <div class="form-group" style="text-align: center;margin-top: 30px;">
                            <button class="btn btn-primary" onclick="return valida_formulario_produto()">Cadastrar</button>
                        </div>
                    </div>   
                    
                </form>
                
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
    <!-- /#page-wrapper -->
    

<?php include_once('includes/footer.php');?>