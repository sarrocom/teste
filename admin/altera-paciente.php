<?php 
    session_start();
    include_once('includes/header.php'); 
    include_once('includes/menu.php');
    include_once('../conexao.php');
    include_once('../sql/select.php');

$id = $_POST['id'];
$pacientes = exibePaciente($conexao, $id); 

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Altera Paciente </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-9">

                <h4 class="page-header text-center">Dados pessoais</h4>
                 <?php if(isset($_SESSION['sucesso'])) { ?>
                    <div class="alert alert-success" role="alert">
                        <strong><?php echo $_SESSION['sucesso'];?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>        
                <?php 
                    unset($_SESSION['sucesso']);
                } ?>
                <?php if(isset($_SESSION['erro'])) { ?>
                    <div class="alert alert-danger" role="alert">
                        <strong><?php echo $_SESSION['erro'];?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>        
                <?php 
                    unset($_SESSION['erro']);
                } ?>

                <?php foreach ($pacientes as $paciente) : 
                   if ($paciente['sexo'] == 'masculino') {
                        $usado = "checked='checked'";
                    } else {
                        $usado = "";
                    }
                    if ($paciente['sexo'] == 'feminino') {
                        $usado1 = "checked='checked'";
                    } else {
                        $usado1 = "";
                    }
                ?>
                    
                    <form name="formularioPaciente" method="post" action="../recebe-forms/alteraPaciente.php">
                        <input type="hidden" id="id" name="id" value="<?=$paciente['id']?>">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nome" id="nome" class="form-control" placeholder="NOME" value="<?=$paciente['nome']?>">
                                <div id="nomeC"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="cpf" id="cpf" class="form-control" placeholder="CPF" value="<?=$paciente['cpf']?>">
                                <div id="cpF"></div>
                            </div>
                            <div class="form-group">
                                <label>SEXO</label>
                                <label class="radio-inline">
                                    <input type="radio" name="sexo" id="masculino" value="masculino" style="height: 20px!important;width: 20px;margin-top: 0px;" <?=$usado?>> Masculino
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="sexo" id="feminino" value="feminino" style="height: 20px!important;width: 20px;margin-top: 0px;" <?=$usado1?>>Feminino
                                </label>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" id="email" class="form-control" placeholder="E-MAIL" value="<?=$paciente['email']?>">
                                <div id="emaiL"></div>
                            </div>
                            <div class="form-group" style="overflow: hidden;">
                                <div class="col-lg-6" style="padding-left: 0;">
                                    <input class="form-control" name="rua" type="text" id="rua" placeholder="ENDEREÇO" value="<?=$paciente['endereco']?>"/>
                                    <div id="enderecO"></div>
                                </div>
                                <div class="col-lg-6" style="padding-right: 0;">
                                    <input class="form-control" name="numero" id="numero" placeholder="NUMERO" value="<?=$paciente['numero']?>">
                                    <div id="numerO"></div>
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <input class="form-control" name="complemento" type="text" id="complemento" placeholder="COMPLEMENTO" value="<?=$paciente['complemento']?>">
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nascimento" id="nascimento" class="form-control" placeholder="DATA NASCIMENTO" value="<?=$paciente['data_nascimento']?>">
                                <div id="nascimentO"></div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="rg" id="rg" class="form-control" placeholder="RG" value="<?=$paciente['rg']?>">
                                        <div id="RG"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="orgaoExpedidor" class="form-control" placeholder="Orgão Expedidor" value="<?=$paciente['orgao_expedidor']?>">
                                        <div id="orgaoExpedidor"></div>
                                    </div>
                                </div>  
                            </div>
                            <div class="form-group">
                                <input type="text" name="telefone" id="telefone" class="form-control" placeholder="TELEFONE" value="<?=$paciente['telefone']?>">
                                <div id="telefonE"></div>
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="cep" type="text" id="cep" size="10" maxlength="9" placeholder="CEP" value="<?=$paciente['cep']?>"/>
                                <div id="ceP"></div>
                            </div>
                        
                            <div class="form-group" style="overflow: hidden;">
                                <div class="col-lg-6" style="padding-left: 0;">
                                    <input class="form-control" name="cidade" type="text" id="cidade" size="40" placeholder="CIDADE" value="<?=$paciente['cidade']?>"/>
                                    <div id="cidadE"></div>
                                </div>
                                <div class="col-lg-6" style="padding-right: 0;">
                                    <input class="form-control" name="uf" type="text" id="uf" size="2" placeholder="ESTADO" value="<?=$paciente['estado']?>"/>
                                    <div id="estadO"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group" style="margin-top: 50px;text-align: center;">
                                <button class="btn btn-primary" onclick="return valida_formulario_paciente()">Altera paciente</button>
                            </div>
                        </div>
                    </form>
                <?php endforeach ?>

            </div>
            <div class="col-lg-3"></div>
        </div>
        
    </div>
    <!-- /#page-wrapper -->

<?php include_once('includes/footer.php');?>