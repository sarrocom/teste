 </div>
    <!-- /#wrapper -->

    <script src="../js/viacep.js"></script>
    <script src="../js/funcoes.js"></script>
    <script src="../js/jquery.maskedinput.min.js"></script>
    <script src="../js/validacao-formularios.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript 
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>
-->
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <script src="../datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR.js"></script>
<script type="text/javascript">
    $('.data_formato').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        language: "pt-BR",
        startDate: '+0d'
    });
</script>
</body>

</html>
