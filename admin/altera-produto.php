<?php 
    session_start();
    include_once('includes/header.php'); 
    include_once('includes/menu.php');
    include_once('../conexao.php');
    include_once('../sql/select.php');

$id = $_POST['id'];
$produtos = exibeProdutos($conexao, $id); 

?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Altera Produto </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-9">
               <?php 
               if(isset($_SESSION['sucesso'])) {  
                    echo $_SESSION['sucesso'];  
                    unset($_SESSION['sucesso']);
                } 
                if(isset($_SESSION['erro'])) { 
                    echo $_SESSION['erro'];
                    unset($_SESSION['erro']);
                } 
                ?>
                
                <?php foreach ($produtos as $produto) : ?>   
                    <form name="formularioProduto" method="post" action="../recebe-forms/alteraProduto.php" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?=$produto['id']?>">
                        <div class="col-lg-9" >
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeProduto" id="nome" class="form-control" placeholder="NOME DO PRODUTO" value="<?=$produto['nome_produto']?>">
                                <div id="produtO"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="nomePrincipioAtivo" id="nomePrincipioAtivo" class="form-control" placeholder="NOME PRINCIPIO ATIVO" value="<?=$produto['nome_principio_ativo']?>">
                                <div id="nomePrincipioAtivo" ></div>
                            </div>  
                            <div class="form-group">
                                <textarea class="form-control" name="descricao" id="descricao" cols="30" rows="10" placeholder="DESCRIÇÃO"><?=$produto['descricao']?></textarea>
                                <div id="descricaO"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeMarca" id="nomeMarca" class="form-control" placeholder="NOME DA MARCA" value="<?=$produto['nome_marca']?>">
                                <div id="nomeMarca"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="preco" id="preco" class="form-control" placeholder="PREÇO SUGERIDO" value="<?=$produto['preco_sugerido']?>">
                                <div id="precO"></div>
                            </div>
                            
                            <div class="form-group">

                                <label for="enfermidades">Enfermidades</label>
                                <select multiple="multiple" class="form-control select" name="enfermidades[]" style="height: 170px;">
                                    <?php
                                    $enfermidades = exibeListaEnfermidade($conexao);
                                    foreach ($enfermidades as $enfermidade) :
                                    ?>
                                    <option value="<?=$enfermidade['nome_enfermidade']?>"><?=$enfermidade['nome_enfermidade']?></option>  
                                    <?php endforeach ?>            
                                </select>
                                <span class="span"></apan>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <h4 class="page-header">Posologia</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="form-group">
                                    <input type="text" name="quantidade_posologia" id="quantidade" class="form-control" placeholder="QUANTIDADE" value="<?=$produto['quantidade_posologia']?>">
                                    <div id="quantidadE"></div>
                                </div>
                               
                                <div class="form-group">
                                    <input type="number" name="quantidadeFrascos_posologia" id="quantidadeFrascos" class="form-control" placeholder="QUANTIDADE DE FRASCOS" value="<?=$produto['quantidadefrascos_posologia']?>">
                                    <div id="quantidadEFrascos"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                             <div class="form-group">
                                <select name="tipoDeIngestao_posologia" class="form-control" style="height: 50px;">
                                    <option value="gotas">Gotas</option>
                                    <option value="miligramas">Miligramas</option>
                                    <option value="centimetros">Centimetros</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="number" name="periodicidade_posologia" id="periodicidade" class="form-control" placeholder="Periodicidade" value="<?=$produto['periodicidade_posologia']?>">
                                <div id="periodicidade"></div>
                            </div>
                        </div>  

                        <div class="col-lg-12">
                            <h4 class="page-header">Fabricante</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeFabricante" id="nomeFabricante" class="form-control" placeholder="nomeFabricante" value="<?=$produto['nome_fabricante']?>">
                                <div id="quantidadE"></div>
                            </div>
                           
                            <div class="form-group">
                                 <input class="form-control" name="ruaFabricante" type="text" id="rua" placeholder="ENDEREÇO" value="<?=$produto['endereco_fabricante']?>"/>
                                <div id="enderecO"></div>
                            </div>

                            <div class="form-group">
                                 <input class="form-control" name="cidadeFabricante" type="text" id="cidade" size="40" placeholder="CIDADE" value="<?=$produto['cidade_fabricante']?>"/>
                                <div id="cidadE"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input class="form-control" name="cepFabricante" type="text" id="cep" size="10" maxlength="9" placeholder="CEP" value="<?=$produto['cep_fabricante']?>"/>
                                <div id="ceP"></div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                 <div class="col-lg-4" style="padding-right: 0">
                                    <input class="form-control" name="numeroFabricante" id="numero" placeholder="Nº" value="<?=$produto['numero_fabricante']?>">
                                    <div id="numerO"></div>
                                 </div>
                                 <div class="col-lg-8">
                                     <input class="form-control" name="complementoFabricante" type="text" id="complemento" value="" placeholder="COMPLEMENTO" value="<?=$produto['complemento_fabricante']?>">
                                 </div>
                                 </div>
                            </div>
                            <div class="form-group">
                                 <input class="form-control" name="ufFabricante" type="text" id="uf" size="2" placeholder="ESTADO" value="<?=$produto['estado_fabricante']?>"/>
                                <div id="estadO"></div>
                            </div>
                        </div>  
                       
                       
                        <div class="col-lg-12">
                            <h4 class="page-header">Exportador</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="nomeExportador" id="nomeFabricante" class="form-control" placeholder="NOME EXPORTADOR" value="<?=$produto['nome_exportador']?>">
                                <div id="quantidadE"></div>
                            </div>
                           
                            <div class="form-group">
                                 <input class="form-control" name="ruaExportador" type="text" id="ruaExportador" placeholder="ENDEREÇO" value="<?=$produto['endereco_exportador']?>"/>
                                <div id="enderecO"></div>
                            </div>

                            <div class="form-group">
                                 <input class="form-control" name="cidadeExportador" type="text" id="cidadeExportador" size="40" placeholder="CIDADE" value="<?=$produto['cidade_exportador']?>"/>
                                <div id="cidadE"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input class="form-control" name="cepExportador" type="text" id="cepExportador" size="10" maxlength="9" placeholder="CEP" value="<?=$produto['cep_exportador']?>"/>
                                <div id="ceP"></div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                     <div class="col-lg-4" style="padding-right: 0">
                                        <input class="form-control" name="numeroExportador" id="numero" placeholder="Nº" value="<?=$produto['numero_exportador']?>">
                                        <div id="numerO"></div>
                                     </div>
                                     <div class="col-lg-8">
                                         <input class="form-control" name="complementoExportador" type="text" id="complementoExportador" value="" placeholder="COMPLEMENTO" value="<?=$produto['complemento_exportador']?>">
                                     </div>
                                </div>
                            </div>
                            <div class="form-group">
                                 <input class="form-control" name="ufExportador" type="text" id="ufExportador" size="2" placeholder="ESTADO" value="<?=$produto['estado_exportador']?>"/>
                                <div id="estadO"></div>
                            </div>
                        </div>
 
                    </div>

                    <div class="col-lg-3" style="margin-top: 50px;">
                        <div class="form-group">
                            <input type="file" name="fotoProduto" id="foto" class="form-control" placeholder="Selecione Sua foto" >
                            <div id="fotO"></div>
                        </div>  
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group" style="text-align: center;margin-top: 30px;">
                            <button class="btn btn-primary" onclick="return valida_formulario_produto()">Alterar</button>
                        </div>
                    </div> 
                    </form>
                <?php endforeach ?>
                
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
    <!-- /#page-wrapper -->
    <script>
        $(".js-example-tokenizer").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
    </script>

<?php include_once('includes/footer.php');?>