<?php

session_start(); 
      
require_once("conexao.php");  
require_once("sql/logica-usuario.php");   

$email = $_POST['email'];
$senha = $_POST['senha'];

$acesso = buscaUsuario($conexao, $email, $senha);

if ($acesso == NULL) {
	$_SESSION['loginErro'] = "<p class='alert alert-danger text-center'>Usuario ou senha inválidos</p>";
	header("Location: index.php");
} else{
	foreach ($acesso as $user) {
		
		if ($user['situacao'] == 1) {
			$_SESSION['nomeUsuario'] = $user['nome'];
			$_SESSION['email'] = $user['email'];
			$_SESSION['idMedico'] = $user['id'];
			header("Location: medico/");
		} elseif ($user['situacao'] == 2){
			$_SESSION['loginErro'] = "<p class='alert alert-danger text-center'>Colaborador</p>";
			header("Location: index.php");
		} else {
			$_SESSION['loginErro'] = "<p class='alert alert-danger text-center'>Usuario</p>";
		header("Location: index.php");
		}
	}
}


die();
