<?php

function deletaMedico($conexao, $id){
	$query = ("DELETE FROM medico WHERE id = '{$id}' ");

	return mysqli_query($conexao, $query);
}

function deletaPaciente($conexao, $id){
	$query = ("DELETE FROM paciente WHERE id = '{$id}' ");

	return mysqli_query($conexao, $query);
}

function deletaProdutos($conexao, $id){
	$query = ("DELETE FROM produtos WHERE id = '{$id}' ");

	return mysqli_query($conexao, $query);
}

function deletaEspecilidade($conexao, $id){
	$query = ("DELETE FROM especialidade WHERE id = '{$id}' ");

	return mysqli_query($conexao, $query);
}

function deletaEnfermidade($conexao, $id){
	$query = ("DELETE FROM enfermidade WHERE id_enfermidade = '{$id}' ");

	return mysqli_query($conexao, $query);
}