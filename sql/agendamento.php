<?php

function agendamentoDia($conexao) {
    $agendamentosDia = array();

    $query = mysqli_query($conexao, "SELECT * FROM agendamento WHERE DAY(data) = DAY(CURDATE()) AND MONTH(data) = MONTH(CURDATE()) AND YEAR(data) = YEAR(CURDATE())");

    while ($row = mysqli_fetch_assoc($query)) {
        array_push($agendamentosDia, $row);
    }


    return $agendamentosDia;

}
function agendamentoMes($conexao) {
    $agendamentosMes = array();

    $query = mysqli_query($conexao, "SELECT * FROM agendamento WHERE MONTH(data) = MONTH(CURDATE()) AND YEAR(data) = YEAR(CURDATE())");

    while ($row = mysqli_fetch_assoc($query)) {
        array_push($agendamentosMes, $row);
    }

    return $agendamentosMes;

}
