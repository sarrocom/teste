<?php 


function alteraMedico($conexao, $id, $nome, $crm, $email, $senha, $cep, $endereco, $numero, $complemento, $cidade, $estado, $cpf,$telefone,$areaAtuacao, $entradaSegunda, $saidaSegunda, $entradaTerca, $saidaTerca, $entradaQuarta, $saidaQuarta, $entradaQuinta, $saidaQuinta, $entradaSexta, $saidaSexta, $entradaSabado, $saidaSabado, $entradaDomingo, $saidaDomingo) {

	$senhaMD5 = md5($senha);

	$query = "UPDATE medico SET 
		nome = '{$nome}',
		crm = '{$crm}',
		email = '{$email}',
		senha = '{$senhaMD5}',
		cep = '{$cep}',
		endereco = '{$endereco}',
		numero = '{$numero}',
		complemento = '{$complemento}',
		cidade = '{$cidade}',
		estado = '{$estado}',
		cpf = '{$cpf}',
		telefone = '{$telefone}',
		area_atuacao = '{$areaAtuacao}',
		segunda_chegada = '{$entradaSegunda}',
		segunda_saida = '{$saidaSegunda}',
		terca_chegada = '{$entradaTerca}',
		terca_saida = '{$saidaTerca}',
		quarta_chegada = '{$entradaQuarta}',
		quarta_saida = '{$saidaQuarta}',
		quinta_chegada = '{$entradaQuinta}',
		quinta_saida = '{$saidaQuinta}',
		sexta_chegada = '{$entradaSexta}',
		sexta_saida = '{$saidaSexta}',
		sabado_chegada = '{$entradaSabado}',
		sabado_saida = '{$saidaSabado}',
		domingo_chegada = '{$entradaDomingo}',
		domingo_saida = '{$saidaDomingo}'
	WHERE id = '{$id}' ";

	return mysqli_query($conexao, $query);
}

function alteraPaciente($conexao, $id, $nome, $nascimento, $cpf, $rg, $orgao_expedidor, $sexo, $telefone, $email, $cep, $endereco, $numero, $cidade, $estado, $complemento){
	$query = "UPDATE paciente SET 
		nome = '{$nome}',
		data_nascimento = '{$nascimento}',
		cpf = '{$cpf}',
		rg = '{$rg}',
		orgao_expedidor = '{$orgao_expedidor}',
		sexo = '{$sexo}',
		telefone = '{$telefone}',
		email = '{$email}',
		cep = '{$cep}',
		endereco = '{$endereco}',
		numero = '{$numero}',
		cidade = '{$cidade}',
		estado = '{$estado}',
		complemento = '{$complemento}'
	WHERE id = '{$id}' ";

	return mysqli_query($conexao, $query);
}

function alteraProduto($conexao, $id, $nome_produto, $noma_marca, $nome_principio_ativo, $preco_sugerido, $descricao, $enfermidade, $foto_produto, $quantidade_posologia, $ingestao_posologia, $quantidadefrascos_posologia, $periodicidade_posologia, $nome_fabricante, $cep_fabricante, $endereco_fabricante, $numero_fabricante, $complemento_fabricante, $cidade_fabricante, $estado_fabricante, $nome_exportador, $cep_exportador, $endereco_exportador, $numero_exportador, $complemento_exportador, $cidade_exportador, $estado_exportador){
	$query = "UPDATE produtos SET 
		nome_produto = '{$nome_produto}',
		nome_marca = '{$noma_marca}',
		nome_principio_ativo = '{$nome_principio_ativo}',
		preco_sugerido = '{$preco_sugerido}',
		descricao = '{$descricao}',
		enfermidade = '{$enfermidade}',
		foto_produto = '{$foto_produto}',
		quantidade_posologia = '{$quantidade_posologia}',
		ingestao_posologia = '{$ingestao_posologia}',
		quantidadefrascos_posologia = '{$quantidadefrascos_posologia}',
		periodicidade_posologia = '{$periodicidade_posologia}',
		nome_fabricante = '{$nome_fabricante}',
		cep_fabricante = '{$cep_fabricante}',
		endereco_fabricante = '{$endereco_fabricante}',
		numero_fabricante = '{$numero_fabricante}',
		complemento_fabricante = '{$complemento_fabricante}',
		cidade_fabricante = '{$cidade_fabricante}',
		estado_fabricante = '{$estado_fabricante}',
		nome_exportador = '{$nome_exportador}',
		cep_exportador = '{$cep_exportador}',
		endereco_exportador = '{$endereco_exportador}',
		numero_exportador = '{$numero_exportador}',
		complemento_exportador = '{$complemento_exportador}',
		cidade_exportador = '{$cidade_exportador}',
		estado_exportador = '{$estado_exportador}'
	WHERE id = '{$id}' ";

	//echo "<br><br>".$query;

	return mysqli_query($conexao, $query);
}

function alteraEspecialidade($conexao, $id, $especialidade){
	$query = "UPDATE especialidade SET 
		especialidade = '{$especialidade}'		
	WHERE id = '{$id}' ";

	echo "<br><br>".$query;

	return mysqli_query($conexao, $query);
}

function alteraEnfermidade($conexao, $id, $enfermidade){
	$query = "UPDATE enfermidade SET 
		nome_enfermidade = '{$enfermidade}'		
	WHERE id_enfermidade = '{$id}' ";

	//echo "<br><br>".$query;

	return mysqli_query($conexao, $query);
}