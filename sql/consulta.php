<?php

// ANOTAÇÕES

function pacienteConsulta($conexao, $cpf) {
	$pacientes = array();
	$query = mysqli_query($conexao, "SELECT * FROM paciente WHERE cpf = '{$cpf}' ");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($pacientes, $row);
	}

	return $pacientes;
}


function cadastroAnotacao($conexao, $nomepaciente, $dataLocal, $CPF, $email, $rg, $anotacao){
	$query = ("INSERT INTO anotacoes(nome_anotacao, data_anotacao, cpf_anotacao, rg_anotacao, anotacao) VALUES ('{$nomepaciente}','{$dataLocal}','{$CPF}','{$rg}', '{$anotacao}')");

	return mysqli_query($conexao, $query);
}


function exibeAnotacoes($conexao){
	$anotacoes = array();
	$query = mysqli_query($conexao, "SELECT * FROM anotacoes ORDER BY id DESC");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($anotacoes, $row);
	}

	return $anotacoes;
}

//fIM ANOTAÇOES

//AVALIAÇÃO MEDICA

function cadastraAvaliacaoMedica($conexao, $nome, $cpf, $enfermidade, $descricao, $tratamento){
	$query = ("INSERT INTO avaliacao_medica(nome_paciente, cpf, enfermidade, descricao_caso, tratamento) VALUES ('{$nome}', '{$cpf}', '{$enfermidade}', '{$descricao}', '{$tratamento}')");

	return mysqli_query($conexao, $query);
}

function exibeAvaliacaomedica($conexao){
	$avaliacaomedica = array();
	$query = mysqli_query($conexao, "SELECT * FROM avaliacao_medica ORDER BY id DESC LIMIT 1");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($avaliacaomedica, $row);
	}

	return $avaliacaomedica;
}