<?php 

$totalPaciente = dashboardPaciente($conexao);
$totalAgendamento = dashboardAgendamento($conexao);
$totalProdutos = dashboardProdutos($conexao);
$totalEnfermidade = dashboardEnfermidade($conexao);


function dashboardPaciente($conexao){
	//seleciona toda a tabela
	$query = mysqli_query($conexao, "SELECT * FROM paciente");
	//Conta quantos registros possuem na tabela
	$total = mysqli_num_rows($query);

	return $total;
}

function dashboardAgendamento($conexao){
	//seleciona toda a tabela
	$query = mysqli_query($conexao, "SELECT * FROM agendamento");
	//Conta quantos registros possuem na tabela
	$total = mysqli_num_rows($query);

	return $total;
}
function dashboardProdutos($conexao){
	//seleciona toda a tabela
	$query = mysqli_query($conexao, "SELECT * FROM produtos");
	//Conta quantos registros possuem na tabela
	$total = mysqli_num_rows($query);

	return $total;
}

function dashboardEnfermidade($conexao){
	//seleciona toda a tabela
	$query = mysqli_query($conexao, "SELECT * FROM enfermidade");
	//Conta quantos registros possuem na tabela
	$total = mysqli_num_rows($query);

	return $total;
}

