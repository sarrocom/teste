<?php 


function cadastroMedico($conexao, $nome, $crm, $email, $senha, $cep, $endereco, $numero, $complemento, $cidade, $estado, $cpf,$telefone,$areaAtuacao,$entradaSegunda,$saidaSegunda, $entradaTerca, $saidaTerca, $entradaQuarta, $saidaQuarta, $entradaQuinta, $saidaQuinta, $entradaSexta, $saidaSexta, $entradaSabado, $saidaSabado, $entradaDomingo, $saidaDomingo){

	$senhaMD5 = md5($senha);

	$query = ("
		INSERT INTO medico
			(nome, crm, email, senha, cep, endereco, numero, complemento, cidade, estado, cpf, telefone, area_atuacao, segunda_chegada, segunda_saida, terca_chegada, terca_saida, quarta_chegada, quarta_saida, quinta_chegada, quinta_saida, sexta_chegada, sexta_saida, sabado_chegada, sabado_saida, domingo_chegada, domingo_saida) 
		VALUES 
			('{$nome}','{$crm}','{$email}','{$senhaMD5}','{$cep}','{$endereco}','{$numero}','{$complemento}','{$cidade}','{$estado}','{$cpf}','{$telefone}',
		'{$areaAtuacao}','{$entradaSegunda}','{$saidaSegunda}','{$entradaTerca}','{$saidaTerca}','{$entradaQuarta}','{$saidaQuarta}','{$entradaQuinta}','{$saidaQuinta}','{$entradaSexta}','{$saidaSexta}','{$entradaSabado}','{$saidaSabado}','{$entradaDomingo}','{$saidaDomingo}')
	");

	return mysqli_query($conexao, $query);
}


function cadastroPaciente($conexao, $nome, $nascimento, $cpf, $rg, $orgao_expedidor, $sexo, $telefone, $email, $cep, $endereco, $numero, $cidade, $estado, $complemento) {
	$query = ("
		INSERT INTO paciente
			(nome, data_nascimento, cpf, rg, orgao_expedidor, sexo, telefone, email, cep, endereco, numero, cidade, estado, complemento) 
		VALUES 
			('{$nome}', '{$nascimento}', '{$cpf}', '{$rg}', '{$orgao_expedidor}', '{$sexo}', '{$telefone}', '{$email}', '{$cep}', '{$endereco}', '{$numero}', '{$cidade}', '{$estado}', '{$complemento}')
	");

	return mysqli_query($conexao, $query);
}

function cadastroProdutos($conexao, $nome_produto, $noma_marca, $nome_principio_ativo, $preco_sugerido, $descricao, $enfermidade, $foto_produto, $quantidade_posologia, $ingestao_posologia, $quantidadefrascos_posologia, $periodicidade_posologia, $nome_fabricante, $cep_fabricante, $endereco_fabricante, $numero_fabricante, $complemento_fabricante, $cidade_fabricante, $estado_fabricante, $nome_exportador, $cep_exportador, $endereco_exportador, $numero_exportador, $complemento_exportador, $cidade_exportador, $estado_exportador){
	$query = ("
		INSERT INTO produtos(nome_produto, nome_marca, nome_principio_ativo, preco_sugerido, descricao, enfermidade, foto_produto, quantidade_posologia, ingestao_posologia, quantidadefrascos_posologia, periodicidade_posologia, nome_fabricante, cep_fabricante, endereco_fabricante, numero_fabricante, complemento_fabricante, cidade_fabricante, estado_fabricante, nome_exportador, cep_exportador, endereco_exportador, numero_exportador, complemento_exportador, cidade_exportador, estado_exportador) 

		VALUES ('{$nome_produto}', '{$noma_marca}', '{$nome_principio_ativo}', '{$preco_sugerido}', '{$descricao}', '{$enfermidade}', '{$foto_produto}', '{$quantidade_posologia}', '{$ingestao_posologia}', '{$quantidadefrascos_posologia}', '{$periodicidade_posologia}', '{$nome_fabricante}', '{$cep_fabricante}', '{$endereco_fabricante}', '{$numero_fabricante}', '{$complemento_fabricante}', '{$cidade_fabricante}', '{$estado_fabricante}', '{$nome_exportador}', '{$cep_exportador}', '{$endereco_exportador}', '{$numero_exportador}', '{$complemento_exportador}', '{$cidade_exportador}', '{$estado_exportador}')
	");

	return mysqli_query($conexao, $query);
}

function cadastroEspecialidade($conexao, $especialidade){
	$query = ("INSERT INTO especialidade (especialidade) VALUES ('{$especialidade}')");

	//echo $query;

	return mysqli_query($conexao, $query);
}

function cadastroEnfermidade($conexao, $enfermidade){
	$query = ("INSERT INTO enfermidade (nome_enfermidade) VALUES ('{$enfermidade}')");

	//echo $query;

	return mysqli_query($conexao, $query);
}