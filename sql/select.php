<?php 

function exibeListaMedicos($conexao){
	$medicos = array();
	$query = mysqli_query($conexao, "SELECT * FROM medico ORDER BY id DESC");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($medicos, $row);
	}

	return $medicos;
}

function exibeMedicos($conexao, $id){
	$medicos = array();
	$query = mysqli_query($conexao, "SELECT * FROM medico WHERE id = '{$id}' ");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($medicos, $row);
	}

	return $medicos;
}

function exibeListaPaciente($conexao){
	$pacientes = array();
	$query = mysqli_query($conexao, "SELECT * FROM paciente ORDER BY id DESC");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($pacientes, $row);
	}

	return $pacientes;
}

function exibePaciente($conexao, $id){
	$pacientes = array();
	$query = mysqli_query($conexao, "SELECT * FROM paciente WHERE id = '{$id}' ");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($pacientes, $row);
	}

	return $pacientes;
}

function exibeListaProdutos($conexao){
	$produtos = array();
	$query = mysqli_query($conexao, "SELECT * FROM produtos ORDER BY id DESC");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($produtos, $row);
	}

	return $produtos;
}

function exibeProdutos($conexao, $id){
	$produtos = array();
	$query = mysqli_query($conexao, "SELECT * FROM produtos WHERE id = '{$id}' ");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($produtos, $row);
	}

	return $produtos;
}


function exibeListaEspecialidade($conexao){
	$pacientes = array();
	$query = mysqli_query($conexao, "SELECT * FROM especialidade ORDER BY id DESC");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($pacientes, $row);
	}

	return $pacientes;
}

function exibeEspecialidade($conexao, $id){
	$pacientes = array();
	$query = mysqli_query($conexao, "SELECT * FROM especialidade WHERE id = '{$id}' ");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($pacientes, $row);
	}

	return $pacientes;
}

function exibeListaEnfermidade($conexao){
	$enfermidade = array();
	$query = mysqli_query($conexao, "SELECT * FROM enfermidade ORDER BY id_enfermidade DESC");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($enfermidade, $row);
	}

	return $enfermidade;
}

function exibeEnfermidade($conexao, $id){
	$enfermidade = array();
	$query = mysqli_query($conexao, "SELECT * FROM enfermidade WHERE id_enfermidade = '{$id}' ");
	while ($row = mysqli_fetch_assoc($query)) {
		array_push($enfermidade, $row);
	}

	return $enfermidade;
}