<?php 

	session_start();
	include_once("includes/header-login.php"); 

?>
	<div class="completa" style="position: fixed; width: 100%; height: 100%; background: #242a34;">

    <div class="header" style="background: white;">
        <nav class="navbar navbar-expand-lg navbar-light barramenu">
            <div class="container" style="    padding: 5px 0;">
                <a class="" href="index.php">
                    <img src="img/logo.jpg" class="logo" alt="Logo Anella">
                </a>                
            </div>
        </nav>
    </div>
	<div class="container">
        <div class="row">
            <?php 
            if (isset($_SESSION['loginErro'])) {
                echo $_SESSION['loginErro'];
                unset($_SESSION['loginErro']);
            }
            ?>
            <div class="col-md-12">
                <h2 style="color:white;text-align: center;    font-size: 2.5rem;">Área exclusiva para médicos</h2>
            </div>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="valida.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control campo-login" placeholder="Login" name="email" type="email" autofocus >
                                </div>
                                <div class="form-group">
                                    <input class="form-control campo-login" placeholder="Senha" name="senha" type="password" >
                                </div>
                                <div class="checkbox" style="color: white;">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Lembre-me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-block botao-login" type="submit">Entrar</button>
                            </fieldset>
                        </form>

                        <p style="color:#cecece;text-align: center; margin-top:50px;font-size: 15px; ">Não possuí uma conta? <span style="color: #06bebd;">envie email para: <a style="color: #06bebd;" href="mailto:contato@anellamed.com?Subject=Cadastro%20Anella%20médicos">contato@anellamed.com</a></span></p>
                    </div>
                </div>
            </div>
        </div>
        <p>
            <?php
                    /*
                if (isset($_SESSION['loginErro'])) {
                    echo $_SESSION['loginErro'];
                    unset($_SESSION['loginErro']);
                }

            ?>
        </p>

        <p>
            <?php
                if (isset($_SESSION['logindeslogado'])) {
                    echo $_SESSION['logindeslogado'];
                    unset($_SESSION['logindeslogado']);
                }
                */
            ?>

        </p>

	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-dialog-centered" style="    max-width: 700px;">
	  		   
	    	<!-- Modal body -->
	    	<div class="modal-body">
	    		<h2>Usuario não localizado</h2>
	    	</div>
	    
	    	<!-- Modal footer -->
	    	<div class="modal-footer">
	      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	    	</div>
	  	</div>
	</div>

    </div>

</div>


<?php include_once ("medico/includes/rodape.php");?>