-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 23-Ago-2018 às 18:06
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anella_sistema`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamento`
--

CREATE TABLE `agendamento` (
  `id_agendamento` int(11) NOT NULL,
  `especialidade` varchar(255) DEFAULT NULL,
  `medico` varchar(255) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `paciente` varchar(255) DEFAULT NULL,
  `idade` varchar(255) DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `rg` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `agendamento`
--

INSERT INTO `agendamento` (`id_agendamento`, `especialidade`, `medico`, `data`, `paciente`, `idade`, `sexo`, `rg`) VALUES
(9, 'teste2', 'Luiz Francisco', '2018-08-09 10:50:56', 'Larissa Lacerda', '22/03/1996', 'Feminino', '22.454.300.1'),
(10, 'teste', 'Lucas Carlos Lacerda ', '2018-08-24 15:00:19', 'Joao teste', '20/08/1990', 'Masculino', '52.164.097-10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `anotacoes`
--

CREATE TABLE `anotacoes` (
  `id` int(11) NOT NULL,
  `nome_anotacao` text,
  `data_anotacao` datetime DEFAULT NULL,
  `cpf_anotacao` varchar(255) DEFAULT NULL,
  `rg_anotacao` varchar(255) DEFAULT NULL,
  `anotacao` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `anotacoes`
--

INSERT INTO `anotacoes` (`id`, `nome_anotacao`, `data_anotacao`, `cpf_anotacao`, `rg_anotacao`, `anotacao`) VALUES
(1, 'Lucas Carlos Lacerda', '0000-00-00 00:00:00', '425.756.458.08', '52.164.097-7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lorem non felis sagittis, non condimentum ex luctus. Donec sagittis neque in sagittis tristique. Nulla ut libero eu nulla pulvinar egestas. Quisque quis placerat magna. Mauris at leo a enim imperdiet mattis. Nam imperdiet, ante et pulvinar tempus, orci ante rutrum turpis, ut lobortis sem metus et lorem. Praesent scelerisque turpis ut augue interdum, vel semper dui rutrum.'),
(2, 'Lucas Carlos Lacerda', '0000-00-00 00:00:00', '425.756.458.08', '52.164.097-7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lorem non felis sagittis, non condimentum ex luctus. Donec sagittis neque in sagittis tristique. Nulla ut libero eu nulla pulvinar egestas. Quisque quis placerat magna. Mauris at leo a enim imperdiet mattis. Nam imperdiet, ante et pulvinar tempus, orci ante rutrum turpis, ut lobortis sem metus et lorem. Praesent scelerisque turpis ut augue interdum, vel semper dui rutrum.'),
(3, 'Lucas Carlos Lacerda', '0000-00-00 00:00:00', '425.756.458.08', '52.164.097-7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lorem non felis sagittis, non condimentum ex luctus. Donec sagittis neque in sagittis tristique. Nulla ut libero eu nulla pulvinar egestas. Quisque quis placerat magna. Mauris at leo a enim imperdiet mattis. Nam imperdiet, ante et pulvinar tempus, orci ante rutrum turpis, ut lobortis sem metus et lorem. Praesent scelerisque turpis ut augue interdum, vel semper dui rutrum.'),
(4, 'Lucas Carlos Lacerda', '0000-00-00 00:00:00', '425.756.458.10', '52.164.097-7', 'Duis ornare, est at egestas imperdiet, nunc eros elementum arcu, sed fermentum nunc lectus ac risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas rhoncus condimentum felis et fermentum. Vivamus id ornare mauris. Nunc cursus semper lacus a porttitor. Duis rhoncus leo nisi, at fringilla ex consequat at. Phasellus sem sapien, sollicitudin vitae finibus non, volutpat et nisl. Nunc fringilla et felis ut sagittis. Quisque egestas lectus sed mi pellentesque cursus. Nullam ipsum urna, consectetur vitae libero vel, ultrices blandit augue. Vestibulum quis orci purus. Donec elementum, purus id semper convallis, elit dolor vehicula elit, nec vehicula ante leo sed magna. Mauris vulputate augue sit amet leo porttitor, sed rhoncus eros facilisis.'),
(5, 'Lucas Carlos Lacerda', '0000-00-00 00:00:00', '425.756.458.10', '52.164.097-7', 'Duis ornare, est at egestas imperdiet, nunc eros elementum arcu, sed fermentum nunc lectus ac risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas rhoncus condimentum felis et fermentum. Vivamus id ornare mauris. Nunc cursus semper lacus a porttitor. Duis rhoncus leo nisi, at fringilla ex consequat at. Phasellus sem sapien, sollicitudin vitae finibus non, volutpat et nisl. Nunc fringilla et felis ut sagittis. Quisque egestas lectus sed mi pellentesque cursus. Nullam ipsum urna, consectetur vitae libero vel, ultrices blandit augue. Vestibulum quis orci purus. Donec elementum, purus id semper convallis, elit dolor vehicula elit, nec vehicula ante leo sed magna. Mauris vulputate augue sit amet leo porttitor, sed rhoncus eros facilisis.'),
(6, 'Lucas Carlos Lacerda', '0000-00-00 00:00:00', '425.756.458.10', '52.164.097-7', 'Duis ornare, est at egestas imperdiet, nunc eros elementum arcu, sed fermentum nunc lectus ac risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas rhoncus condimentum felis et fermentum. Vivamus id ornare mauris. Nunc cursus semper lacus a porttitor. Duis rhoncus leo nisi, at fringilla ex consequat at. Phasellus sem sapien, sollicitudin vitae finibus non, volutpat et nisl. Nunc fringilla et felis ut sagittis. Quisque egestas lectus sed mi pellentesque cursus. Nullam ipsum urna, consectetur vitae libero vel, ultrices blandit augue. Vestibulum quis orci purus. Donec elementum, purus id semper convallis, elit dolor vehicula elit, nec vehicula ante leo sed magna. Mauris vulputate augue sit amet leo porttitor, sed rhoncus eros facilisis.'),
(7, 'Larissa Lacerda', '0000-00-00 00:00:00', '555.333.222.11', '22.454.300.1', 'anotacao de larissa lacerda 01'),
(8, 'Larissa Lacerda', '2018-08-09 15:18:29', '555.333.222.11', '22.454.300.1', 'anotacao de larissa lacerda 02'),
(9, 'Larissa Lacerda', '2018-08-09 15:40:00', '555.333.222.11', '22.454.300.1', 'Mais uma anotaÃ§Ã£o sobre o estado da paciente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao_medica`
--

CREATE TABLE `avaliacao_medica` (
  `id` int(11) NOT NULL,
  `nome_paciente` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `enfermidade` varchar(255) DEFAULT NULL,
  `descricao_caso` text,
  `tratamento` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `avaliacao_medica`
--

INSERT INTO `avaliacao_medica` (`id`, `nome_paciente`, `cpf`, `enfermidade`, `descricao_caso`, `tratamento`) VALUES
(1, 'Larissa Lacerda', '555.333.222.11', 'Enfermidade_01', 'desc', 'trat'),
(2, '', '', 'Enfermidade_01', 'desc', 'tratamento'),
(3, '', '', 'Enfermidade_01', 'descriÃ§Ã£o do medico ', 'tratamento'),
(4, '', '555.333.222.11', 'Enfermidade_01', 'descriÃ§Ã£o do mÃ©dico ', 'tratamento'),
(5, '', '', 'Enfermidade_01', 'descriÃ§Ã£o do mÃ©dico gh', 'tratamento');

-- --------------------------------------------------------

--
-- Estrutura da tabela `enfermidade`
--

CREATE TABLE `enfermidade` (
  `id_enfermidade` int(11) NOT NULL,
  `nome_enfermidade` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `enfermidade`
--

INSERT INTO `enfermidade` (`id_enfermidade`, `nome_enfermidade`) VALUES
(3, 'Teste novo4654564'),
(4, 'Enfermidade_01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `especialidade`
--

CREATE TABLE `especialidade` (
  `id` int(11) NOT NULL,
  `especialidade` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `especialidade`
--

INSERT INTO `especialidade` (`id`, `especialidade`) VALUES
(1, 'teste'),
(3, 'teste2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(220) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `especialidade` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `events`
--

INSERT INTO `events` (`id`, `title`, `color`, `start`, `end`, `especialidade`) VALUES
(3, 'Reuniao 1', '#FFD700', '2018-06-23 08:00:00', '2018-06-23 09:00:00', 'teste'),
(9, 'ReuniÃ£o sobre anella Sistema ', '#8B0000', '2018-06-07 10:30:00', '2018-06-07 12:00:00', 'teste'),
(10, 'Primero Evento', '#436EEE', '2018-06-18 15:30:50', '2018-06-20 18:00:00', 'teste'),
(11, 'Add Map Step 2', '#FFD700', '2018-06-06 09:00:00', '2018-06-07 15:00:00', 'teste'),
(12, 'Primero Evento', '#0071c5', '2018-08-01 22:00:00', '2018-08-02 23:05:20', 'oftamologista');

-- --------------------------------------------------------

--
-- Estrutura da tabela `horarios`
--

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL,
  `estabelecimento` varchar(220) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `horarios`
--

INSERT INTO `horarios` (`id`, `estabelecimento`, `data`) VALUES
(1, 'Mercado', '2017-09-10 09:45:44'),
(2, 'Farmacia', '2017-09-23 09:00:32'),
(3, 'Quitanda', '2018-08-07 22:10:59'),
(4, 'Quitanda do Bahia', '2018-09-17 14:00:43'),
(5, '', '2018-09-22 14:00:33'),
(6, 'Quitanda do Bahia', '2018-08-26 19:30:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medico`
--

CREATE TABLE `medico` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `crm` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `cep` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `estado` varchar(5) NOT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `area_atuacao` varchar(50) DEFAULT NULL,
  `segunda_chegada` varchar(20) NOT NULL,
  `segunda_saida` varchar(20) NOT NULL,
  `terca_chegada` varchar(20) NOT NULL,
  `terca_saida` varchar(20) NOT NULL,
  `quarta_chegada` varchar(20) NOT NULL,
  `quarta_saida` varchar(20) NOT NULL,
  `quinta_chegada` varchar(20) NOT NULL,
  `quinta_saida` varchar(20) NOT NULL,
  `sexta_chegada` varchar(20) NOT NULL,
  `sexta_saida` varchar(20) NOT NULL,
  `sabado_chegada` varchar(20) NOT NULL,
  `sabado_saida` varchar(20) NOT NULL,
  `domingo_chegada` varchar(20) NOT NULL,
  `domingo_saida` varchar(20) NOT NULL,
  `situacao` varchar(5) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `medico`
--

INSERT INTO `medico` (`id`, `nome`, `crm`, `email`, `senha`, `cep`, `endereco`, `numero`, `complemento`, `cidade`, `estado`, `cpf`, `telefone`, `area_atuacao`, `segunda_chegada`, `segunda_saida`, `terca_chegada`, `terca_saida`, `quarta_chegada`, `quarta_saida`, `quinta_chegada`, `quinta_saida`, `sexta_chegada`, `sexta_saida`, `sabado_chegada`, `sabado_saida`, `domingo_chegada`, `domingo_saida`, `situacao`) VALUES
(5, 'Lucas Carlos Lacerda ', '4564132', 'lacerda.lucasc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '08692090', 'Rua Avelino Teixeira', '52', 'NÃ£o hÃ¡ complemento', 'Suzano', 'SP', '425.756.458.08', '(11) 9835-', 'atuacao3', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(6, 'Luiz Francisco', '84431324', 'luiz@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '09941-640', 'Rua Annita', '52', 'NÃ£o hÃ¡ complemento', 'Diadema', 'SP', '145.456.465.46', '(99) 9999-9999', 'teste2', '08:00', '18:00', '08:00', '18:00', '08:00', '18:00', '08:00', '18:00', '08:00', '18:00', '08:00', '18:00', '08:00', '18:00', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paciente`
--

CREATE TABLE `paciente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `data_nascimento` varchar(255) DEFAULT NULL,
  `cpf` varchar(50) DEFAULT NULL,
  `rg` varchar(50) DEFAULT NULL,
  `orgao_expedidor` varchar(20) DEFAULT NULL,
  `sexo` varchar(20) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cep` varchar(50) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `paciente`
--

INSERT INTO `paciente` (`id`, `nome`, `data_nascimento`, `cpf`, `rg`, `orgao_expedidor`, `sexo`, `telefone`, `email`, `cep`, `endereco`, `numero`, `cidade`, `estado`, `complemento`) VALUES
(8, 'lacerda', '22/03/1996', '744.545.454.54', '54.657.498.7', '', 'masculino', '(11) 4749-8595', 'web@sarro.com.br', '', '', '', '', '', 'NÃ£o hÃ¡ complemento'),
(9, 'Larissa Lacerda', '03/10/1997', '555.333.222.11', '22.454.300.1', 'SSP', 'feminino', '(54) 6413-2131', 'larissalacerda@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Lucas Carlos Lacerda', '22/03/1996', '425.756.458.10', '52.164.097-7', 'ssp', 'masculino', '(11) 4749-8595', 'lacerda.lucasc@gmail.com', '08692090', 'Rua Avelino Teixeira', '25', 'Suzano', 'SP', 'NÃ£o hÃ¡ complemento');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `nome_produto` varchar(255) DEFAULT NULL,
  `nome_marca` varchar(255) DEFAULT NULL,
  `nome_principio_ativo` varchar(255) DEFAULT NULL,
  `preco_sugerido` varchar(255) DEFAULT NULL,
  `descricao` text,
  `enfermidade` varchar(255) DEFAULT NULL,
  `foto_produto` varchar(255) DEFAULT NULL,
  `quantidade_posologia` varchar(20) DEFAULT NULL,
  `ingestao_posologia` varchar(255) DEFAULT NULL,
  `quantidadefrascos_posologia` varchar(20) DEFAULT NULL,
  `periodicidade_posologia` varchar(255) DEFAULT NULL,
  `nome_fabricante` varchar(255) DEFAULT NULL,
  `cep_fabricante` varchar(30) DEFAULT NULL,
  `endereco_fabricante` varchar(255) DEFAULT NULL,
  `numero_fabricante` varchar(10) DEFAULT NULL,
  `complemento_fabricante` varchar(500) DEFAULT NULL,
  `cidade_fabricante` varchar(50) DEFAULT NULL,
  `estado_fabricante` varchar(5) DEFAULT NULL,
  `nome_exportador` varchar(255) DEFAULT NULL,
  `cep_exportador` varchar(30) DEFAULT NULL,
  `endereco_exportador` varchar(255) DEFAULT NULL,
  `numero_exportador` varchar(10) DEFAULT NULL,
  `complemento_exportador` varchar(500) DEFAULT NULL,
  `cidade_exportador` varchar(50) DEFAULT NULL,
  `estado_exportador` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome_produto`, `nome_marca`, `nome_principio_ativo`, `preco_sugerido`, `descricao`, `enfermidade`, `foto_produto`, `quantidade_posologia`, `ingestao_posologia`, `quantidadefrascos_posologia`, `periodicidade_posologia`, `nome_fabricante`, `cep_fabricante`, `endereco_fabricante`, `numero_fabricante`, `complemento_fabricante`, `cidade_fabricante`, `estado_fabricante`, `nome_exportador`, `cep_exportador`, `endereco_exportador`, `numero_exportador`, `complemento_exportador`, `cidade_exportador`, `estado_exportador`) VALUES
(2, 'Dipirona', 'teste', 'Dipirona', '12.00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus laoreet justo augue, non venenatis dui cursus in. Nulla consequat tincidunt odio convallis finibus. Vestibulum fermentum velit suscipit, lobortis libero ac, pharetra eros. Nam eget mauris sit amet ex accumsan viverra. Maecenas congue neque in nisi maximus gravida. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ultricies mauris non elementum euismod. Fusce iaculis turpis fermentum condimentum posuere. Nulla vitae elementum erat. Aenean ut nisi at diam consectetur molestie. Vivamus dictum eu urna non porta. Aenean gravida nisl id odio lobortis accumsan. Donec et porttitor nisl. Vestibulum nec magna pulvinar, porta sem ac, ultrices ante. Morbi pellentesque nulla id quam ullamcorper vulputate. Praesent efficitur lacus risus, et dictum erat convallis vitae.', 'teste2, teste', 'buildings-venice_00370298.jpg', '10', 'gotas', '5', '4', 'Empresa DIpi', '08692090', 'Rua Avelino Teixeira', '4656', 'NÃ£o hÃ¡ complemento', 'Suzano', 'SP', 'expo Dipi', '08692090', 'Rua Avelino Teixeira', '2313', 'NÃ£o hÃ¡ complemento', 'Suzano', 'SP');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agendamento`
--
ALTER TABLE `agendamento`
  ADD PRIMARY KEY (`id_agendamento`);

--
-- Indexes for table `anotacoes`
--
ALTER TABLE `anotacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avaliacao_medica`
--
ALTER TABLE `avaliacao_medica`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enfermidade`
--
ALTER TABLE `enfermidade`
  ADD PRIMARY KEY (`id_enfermidade`);

--
-- Indexes for table `especialidade`
--
ALTER TABLE `especialidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agendamento`
--
ALTER TABLE `agendamento`
  MODIFY `id_agendamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `anotacoes`
--
ALTER TABLE `anotacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `avaliacao_medica`
--
ALTER TABLE `avaliacao_medica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `enfermidade`
--
ALTER TABLE `enfermidade`
  MODIFY `id_enfermidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `especialidade`
--
ALTER TABLE `especialidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `medico`
--
ALTER TABLE `medico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
