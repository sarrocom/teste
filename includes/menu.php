	<div class="header">
		<nav class="navbar navbar-expand-lg navbar-light barramenu">
			<div class="container">
				<a class="navbar-brand" href="index.php">
					<img src="../img/logo.jpg" class="logo" alt="Logo Anella">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="nav navbar-nav ml-auto">
					  <li class="nav-item active">
					    <a class="nav-link" href="index.php">Home 
						
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="agenda.php">Agenda</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="pacientes.php">Pacientes</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="consultas.php">Consultas</a>
					  </li>
					   <li class="nav-item">
					    <a class="nav-link" href="produtos.php">Produtos</a>
					  </li>
					   <li class="nav-item">
					    <a class="nav-link disabled" href="#">Ajuda</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link disabled" href="#">Contato</a>
					  </li>
					  
					</ul>
				</div>
			</div>
		</nav>
	</div>

