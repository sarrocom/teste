<?php
session_start();
//Conexao com BD
include_once("../conexao.php");

//Receber os dados do formulário

$especialidade = $_REQUEST['especialidade'];
$medico = $_REQUEST['medicoAgendamento'];
$data = $_REQUEST['data'];
$paciente = $_REQUEST['pacienteAgendamento'];
$idade = $_REQUEST['idadeAgendamento'];
$sexo = $_REQUEST['sexoAgendamento'];
$rg = $_REQUEST['rgAgendamento'];

//Converter a data e hora do formato brasileiro para o formato do Banco de Dados
$data = explode(" ", $data);
list($date, $hora) = $data;
$data_sem_barra = array_reverse(explode("/", $date));
$data_sem_barra = implode("-", $data_sem_barra);
$data_sem_barra = $data_sem_barra . " " . $hora;

echo $data_sem_barra.'<br>';
echo $estabelecimento;


//Salvar no BD
$result_data = "INSERT INTO  agendamento(especialidade, medico, data, paciente, idade, sexo, rg) 
VALUES ('{$especialidade}', '{$medico}', '{$data_sem_barra}', '{$paciente}', '{$idade}', '{$sexo}', '{$rg}')";
$resultado_data = mysqli_query($conexao, $result_data);

//Verificar se salvou no banco de dados através "mysqli_insert_id" o qual verifica se existe o ID do último dado inserido
if(mysqli_insert_id($conexao)){
	$_SESSION['msg'] = "<div class='alert alert-success'> Data cadastrada com sucesso </div>";
	header("Location: ../admin/agenda.php");
}else{
	$_SESSION['msg'] = "<div class='alert alert-danger'> Erro ao cadastradar a data </div>";
	header("Location: ../admin/agenda.php");
}

