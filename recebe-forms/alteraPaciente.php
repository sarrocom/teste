<?php
session_start();
include_once("../conexao.php");
include_once("../sql/update.php");

$id = $_POST['id'];
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); 
$nascimento = mysqli_real_escape_string($conexao, $_POST['nascimento']);
$cpf = mysqli_real_escape_string($conexao, $_POST['cpf']);
$rg = mysqli_real_escape_string($conexao, $_POST['rg']); 
$orgao_expedidor = mysqli_real_escape_string($conexao, $_POST['orgaoExpedidor']);
$sexo = mysqli_real_escape_string($conexao, $_POST['sexo']); 
$telefone = mysqli_real_escape_string($conexao, $_POST['telefone']);
$email = mysqli_real_escape_string($conexao, $_POST['email']); 
$cep = mysqli_real_escape_string($conexao, $_POST['cep']);
$endereco = mysqli_real_escape_string($conexao, $_POST['rua']);
$numero = mysqli_real_escape_string($conexao, $_POST['numero']);
$cidade = mysqli_real_escape_string($conexao, $_POST['cidade']);
$estado = mysqli_real_escape_string($conexao, $_POST['uf']);
if ($_POST['complemento'] == '') {
	$complemento = 'Não há complemento';
} else {
	$complemento = $_POST['complemento'];
}


if (alteraPaciente($conexao, $id, $nome, $nascimento, $cpf, $rg, $orgao_expedidor, $sexo, $telefone, $email, $cep, $endereco, $numero, $cidade, $estado, $complemento)) {

	$_SESSION['sucesso'] = "Alterou com Sucesso.";
	header("Location: ../admin/lista-pacientes.php");

} else {

	$_SESSION['erro'] = "Houve algum erro, tente novamente.";
	header("Location: ../admin/lista-pacientes.php");
	
}

