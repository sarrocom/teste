<?php
session_start();
include_once('../conexao.php');
include_once('../sql/delete.php');

$id = $_POST['id'];

if (deletaEnfermidade($conexao, $id)) {

	$_SESSION['sucesso'] = "Deletado com sucesso.";
	header("Location: ../admin/lista-enfermidade.php");

} else {

	$_SESSION['erro'] = "Houve algum erro, tente novamente.";
	header("Location: ../admin/lista-enfermidade.php");

}

die();