<?php
session_start();
require_once("../conexao.php");
require_once("../sql/insert.php");

$nome_produto = mysqli_real_escape_string($conexao, $_POST['nomeProduto']);
$noma_marca = mysqli_real_escape_string($conexao, $_POST['nomeMarca']);
$nome_principio_ativo = mysqli_real_escape_string($conexao, $_POST['nomePrincipioAtivo']);
$preco_sugerido = mysqli_real_escape_string($conexao, $_POST['preco']);
$descricao = mysqli_real_escape_string($conexao, $_POST['descricao']);
$foto_produto = mysqli_real_escape_string($conexao, $_FILES['fotoProduto']['name']);

$diretorio = '../fotos/';
$uploadfile = $diretorio . basename($foto_produto);
if (move_uploaded_file($_FILES['fotoProduto']['tmp_name'], $uploadfile)) {
	 echo "Moveu com sucesso<br><br>";
} else {
    echo "Possível ataque de upload de foto!\n<br><br>";
}

$enfermidades = $_POST['enfermidades']; //Fazer uma validação de array
$enfermidade = '';
foreach($enfermidades as $v){
    $enfermidade .= $v . ", ";
}
$enfermidade = trim($enfermidade, ', '); // Use esta variável para gravar no banco.

$quantidade_posologia = mysqli_real_escape_string($conexao, $_POST['quantidade_posologia']);
$ingestao_posologia = mysqli_real_escape_string($conexao, $_POST['tipoDeIngestao_posologia']);
$quantidadefrascos_posologia = mysqli_real_escape_string($conexao, $_POST['quantidadeFrascos_posologia']);
$periodicidade_posologia = mysqli_real_escape_string($conexao, $_POST['periodicidade_posologia']);
$nome_fabricante = mysqli_real_escape_string($conexao, $_POST['nomeFabricante']);
$cep_fabricante = mysqli_real_escape_string($conexao, $_POST['cepFabricante']);
$endereco_fabricante = mysqli_real_escape_string($conexao, $_POST['ruaFabricante']);
$numero_fabricante = mysqli_real_escape_string($conexao, $_POST['numeroFabricante']);
$cidade_fabricante = mysqli_real_escape_string($conexao, $_POST['cidadeFabricante']);
$estado_fabricante = mysqli_real_escape_string($conexao, $_POST['ufFabricante']);
$nome_exportador = mysqli_real_escape_string($conexao, $_POST['nomeExportador']);
$cep_exportador = mysqli_real_escape_string($conexao, $_POST['cepExportador']);
$endereco_exportador = mysqli_real_escape_string($conexao, $_POST['ruaExportador']);
$numero_exportador = mysqli_real_escape_string($conexao, $_POST['numeroExportador']);
$cidade_exportador = mysqli_real_escape_string($conexao, $_POST['cidadeExportador']);
$estado_exportador = mysqli_real_escape_string($conexao, $_POST['ufExportador']);
if ($_POST['complementoFabricante'] == '') {
	$complemento_fabricante = 'Não há complemento';
} else {
	$complemento_fabricante = $_POST['complementoFabricante'];
}

if ($_POST['complementoExportador'] == '') {
	$complemento_exportador = 'Não há complemento';
} else {
	$complemento_exportador = $_POST['complementoExportador'];
}

if (cadastroProdutos($conexao, $nome_produto, $noma_marca, $nome_principio_ativo, $preco_sugerido, $descricao, $enfermidade, $foto_produto, $quantidade_posologia, $ingestao_posologia, $quantidadefrascos_posologia, $periodicidade_posologia, $nome_fabricante, $cep_fabricante, $endereco_fabricante, $numero_fabricante, $complemento_fabricante, $cidade_fabricante, $estado_fabricante, $nome_exportador, $cep_exportador, $endereco_exportador, $numero_exportador, $complemento_exportador, $cidade_exportador, $estado_exportador)) {

	$_SESSION['sucesso'] = "<div class='alert alert-success' role='alert'><strong>Cadastrou com Sucesso.</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div> ";
	header("Location: ../admin/cadastro-produto.php");

} else {

	$_SESSION['erro'] = "<div class='alert alert-danger' role='alert'><strong>Houve algum erro, tente novamente.</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div> ";
	header("Location: ../admin/cadastro-produto.php");
	
}


















/*
"

"

*/