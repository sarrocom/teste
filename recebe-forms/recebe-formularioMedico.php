<?php
session_start();
include_once("../conexao.php");
include_once("../sql/insert.php");

$nome = mysqli_real_escape_string($conexao, $_POST['nome']);
$crm = mysqli_real_escape_string($conexao, $_POST['crm']);
$email = mysqli_real_escape_string($conexao, $_POST['email']);
$senha = mysqli_real_escape_string($conexao, $_POST['senha']);
$cep = mysqli_real_escape_string($conexao, $_POST['cep']);
$endereco = mysqli_real_escape_string($conexao, $_POST['rua']); 
$numero = mysqli_real_escape_string($conexao, $_POST['numero']);
$cidade = mysqli_real_escape_string($conexao, $_POST['cidade']);
$estado = mysqli_real_escape_string($conexao, $_POST['uf']);
$cpf = mysqli_real_escape_string($conexao, $_POST['cpf']);
$telefone = mysqli_real_escape_string($conexao, $_POST['telefone']);
$areaAtuacao = mysqli_real_escape_string($conexao, $_POST['areaAtuacao']);
$entradaSegunda = mysqli_real_escape_string($conexao, $_POST['horaInicioSegunda']);
$saidaSegunda = mysqli_real_escape_string($conexao, $_POST['horaFimSegunda']); 
$entradaTerca = mysqli_real_escape_string($conexao, $_POST['horaInicioTerca']);
$saidaTerca = mysqli_real_escape_string($conexao, $_POST['horaFimTerca']);
$entradaQuarta = mysqli_real_escape_string($conexao, $_POST['horaInicioQuarta']);
$saidaQuarta = mysqli_real_escape_string($conexao, $_POST['horaFimQuarta']);
$entradaQuinta = mysqli_real_escape_string($conexao, $_POST['horaInicioQuinta']);
$saidaQuinta = mysqli_real_escape_string($conexao, $_POST['horaFimQuinta']);
$entradaSexta = mysqli_real_escape_string($conexao, $_POST['horaInicioSexta']);
$saidaSexta = mysqli_real_escape_string($conexao, $_POST['horaFimSexta']);
$entradaSabado = mysqli_real_escape_string($conexao, $_POST['horaInicioSabado']);
$saidaSabado = mysqli_real_escape_string($conexao, $_POST['horaFimSabado']);
$entradaDomingo = mysqli_real_escape_string($conexao, $_POST['horaInicioDomingo']);
$saidaDomingo = mysqli_real_escape_string($conexao, $_POST['horaFimDomingo']);
if ($_POST['complemento'] == '') {
	$complemento = 'Não há complemento';
} else {
	$complemento = $_POST['complemento'];
}


if(cadastroMedico($conexao, $nome, $crm, $email, $senha, $cep, $endereco, $numero, $complemento, $cidade, $estado, $cpf,$telefone,$areaAtuacao, $entradaSegunda, $saidaSegunda, $entradaTerca, $saidaTerca, $entradaQuarta, $saidaQuarta, $entradaQuinta, $saidaQuinta, $entradaSexta, $saidaSexta, $entradaSabado, 
	$saidaSabado, $entradaDomingo, $saidaDomingo)){

	$_SESSION['sucesso'] = "Cadastrou com Sucesso.";
	header("Location: ../admin/cadastro-medico.php");

} else {

	$_SESSION['erro'] = "Houve algum erro, tente novamente.";
	header("Location: ../admin/cadastro-medico.php");
	
}


