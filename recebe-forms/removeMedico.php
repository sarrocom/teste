<?php
session_start();
include_once('../conexao.php');
include_once('../sql/delete.php');

$id = $_POST['id'];

if (deletaMedico($conexao, $id)) {

	$_SESSION['sucesso'] = "Deletado com sucesso.";
	header("Location: ../admin/lista-medicos.php");

} else {

	$_SESSION['erro'] = "Houve algum erro, tente novamente.";
	header("Location: ../admin/lista-medicos.php");

}

die();