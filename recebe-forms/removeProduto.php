<?php
session_start();
include_once('../conexao.php');
include_once('../sql/delete.php');

$id = $_POST['id'];

if (deletaProdutos($conexao, $id)) {
	$_SESSION['sucesso'] = "<div class='alert alert-success' role='alert'><strong>Deletado com sucesso.</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden="true">&times;</span></button></div> ";
	header("Location: ../admin/lista-produtos.php");

} else {

	$_SESSION['erro'] = "<div class='alert alert-danger' role='alert'><strong>Houve algum erro, tente novamente.</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden="true">&times;</span></button></div> ";
	header("Location: ../admin/lista-produtos.php");

}

die();