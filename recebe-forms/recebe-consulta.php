<?php

session_start();
require_once("../conexao.php");
require_once('../sql/consulta.php');

$nomepaciente = $_POST['nome'];
$CPF = $_POST['cpf'];
$email = $_POST['email'];
$rg = $_POST['rg'];
$anotacao = $_POST['anotacoes'];
$dataLocal = date('Y-m-d H:i:s', time());
date_default_timezone_set('America/Sao_Paulo');


if (cadastroAnotacao($conexao, $nomepaciente, $dataLocal, $CPF, $email, $rg, $anotacao)) {

	$_SESSION['msg'] = "<div class='alert alert-success' role='alert'><strong>Cadastrou com Sucesso.</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div> ";

	$_SESSION['cpf'] = $CPF;

	header("Location: ../medico/consultas.php");

} else {
	$_SESSION['msg'] = "<div class='alert alert-danger' role='alert'><strong>Houve algum erro, tente novamente.</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div> ";
	$_SESSION['cpf'] = $CPF;
	header("Location: ../medico/consultas.php");
	
} 

